# vaadin-ant-design

Vaadin wrapper for https://ant.design/components/overview/

# Heroku

Showcase available at https://vaadin-ant-design-showcase.herokuapp.com/

# Plan to develop

- Functions (callbacks and simple functions)
- Data Entry
- Data Display
- Feedback
- Other
