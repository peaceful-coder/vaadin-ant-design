/**
 * This file has been autogenerated as it didn't exist or was made for an older
 * incompatible version.
 *
 * This file can be used for manual configuration. It will not be modified
 * if the flowDefaults constant exists.
 */
const merge = require('webpack-merge');
const flowDefaults = require('./webpack.generated.js');

/**
 * To change the webpack config, add a new configuration object in
 * the merge arguments below:
 */
module.exports = merge(flowDefaults,
    //Saving source message in exceptions
    {
        mode: 'development',
        devtool: 'inline-source-map',
    },

    // Add a custom plugin
    // (install the plugin with `npm install --save-dev webpack-bundle-analyzer`)
    // {
    //   plugins: [
    //     new require('webpack-bundle-analyzer').BundleAnalyzerPlugin({
    //       analyzerMode: 'static'
    //     })
    //   ]
    // },
    {
        module: {
            rules: [
                {
                    test: /\.jsx$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.svg$/,
                    use: ['@svgr/webpack'],
                },
            ]
        }
    }
);
