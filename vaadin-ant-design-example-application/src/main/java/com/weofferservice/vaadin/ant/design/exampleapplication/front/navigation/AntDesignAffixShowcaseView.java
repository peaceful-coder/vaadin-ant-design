package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButton;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.navigation.affix.AntDesignAffix;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.UUID;
import java.util.stream.IntStream;

/**
 * @author Peaceful Coder
 */
@Route("affix")
public class AntDesignAffixShowcaseView extends VerticalLayout {

	public AntDesignAffixShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addAffixTop();
		addAffixBottom();
	}

	private void addAffixBottom() {
		final String title = "Affix Bottom";
		add(new Label(title));
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		IntStream.range(0, 100).forEach(value ->
				showcase.add(new Div(new Label("Filler")))
		);
		final AntDesignButton child = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN))
				.buttonType(AntDesignButtonType.PRIMARY)
				.turnOnClick()
				.build();
		final AntDesignAffix affix = AntDesignAffix.builder()
				.offsetBottom(50)
				.addChild(child)
				.build();
		affix.addListener(AntDesignOnClickEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		showcase.add(affix);
		add(showcase);
	}

	private void addAffixTop() {
		final String title = "Affix Top";
		add(new Label(title));
		final String showcaseId = UUID.randomUUID().toString();
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		showcase.setHeight("250px");
		showcase.getStyle().set("border", "5px solid black");
		showcase.getStyle().set("overflow-y", "auto");
		showcase.setId(showcaseId);
		final Div div = new Div();
		div.getStyle().set("min-height", "500px");
		div.getStyle().set("border", "5px solid red");
		showcase.add(div);
		add(showcase);
		final AntDesignButton child = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN))
				.buttonType(AntDesignButtonType.PRIMARY)
				.turnOnClick()
				.build();
		final AntDesignAffix affix = AntDesignAffix.builder()
				.offsetTop(50)
				.target(showcase)
				.addChild(child)
				.build();
		affix.addListener(AntDesignOnClickEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		div.add(affix);
	}
}
