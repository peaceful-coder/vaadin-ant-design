package com.weofferservice.vaadin.ant.design.exampleapplication.front.layout;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.layout.layout.AntDesignLayout;
import com.weofferservice.vaadin.ant.design.components.layout.layout.AntDesignLayoutType;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

/**
 * @author Peaceful Coder
 */
@Route("layout")
public class AntDesignLayoutShowcaseView extends VerticalLayout {

	public AntDesignLayoutShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasicStructure();
		addBasicStructureWithLeftSider();
		addBasicStructureWithRightSider();
		addLeftSiderWithBasicStructure();
	}

	private void addLeftSiderWithBasicStructure() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignLayout sider = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.SIDER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Sider")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout header = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.HEADER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Header")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout content = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.CONTENT)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Content")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout footer = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.FOOTER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Footer")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout basicStructure = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(header)
				.addChild(content)
				.addChild(footer)
				.build();
		final AntDesignLayout layout = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(sider)
				.addChild(basicStructure)
				.build();
		showcase.add(layout);
		add(new Label("Left Sider With Basic Structure"));
		add(showcase);
	}

	private void addBasicStructureWithRightSider() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignLayout sider = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.SIDER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Sider")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout content = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.CONTENT)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Content")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout contentSider = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(content)
				.addChild(sider)
				.build();
		final AntDesignLayout header = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.HEADER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Header")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout footer = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.FOOTER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Footer")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout layout = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(header)
				.addChild(contentSider)
				.addChild(footer)
				.build();
		showcase.add(layout);
		add(new Label("Layout With Right Sider"));
		add(showcase);
	}

	private void addBasicStructureWithLeftSider() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignLayout sider = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.SIDER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Sider")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout content = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.CONTENT)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Content")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout siderContent = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(sider)
				.addChild(content)
				.build();
		final AntDesignLayout header = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.HEADER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Header")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout footer = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.FOOTER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Footer")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout layout = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(header)
				.addChild(siderContent)
				.addChild(footer)
				.build();
		showcase.add(layout);
		add(new Label("Layout With Left Sider"));
		add(showcase);
	}

	private void addBasicStructure() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignLayout header = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.HEADER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Header")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout content = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.CONTENT)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Content")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout footer = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.FOOTER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Footer")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout layout = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(header)
				.addChild(content)
				.addChild(footer)
				.build();
		showcase.add(layout);
		add(new Label("Layout"));
		add(showcase);
	}
}
