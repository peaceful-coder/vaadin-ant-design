package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.general.typography.title.AntDesignTitle;

/**
 * @author Peaceful Coder
 */
@Route("typography-title")
public class AntDesignTypographyTitleShowcaseView extends VerticalLayout {

	public AntDesignTypographyTitleShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Typography Title Showcase
		final FlexLayout titleShowcase = new FlexLayout();
		titleShowcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		final AntDesignTitle titleDefault = AntDesignTitle.builder()
				.text("Title Default")
				.build();
		final AntDesignTitle titleH1 = AntDesignTitle.builder()
				.text("Title H1")
				.level(1)
				.build();
		final AntDesignTitle titleH2 = AntDesignTitle.builder()
				.text("Title H2")
				.level(2)
				.build();
		final AntDesignTitle titleH3 = AntDesignTitle.builder()
				.text("Title H3")
				.level(3)
				.build();
		final AntDesignTitle titleH4 = AntDesignTitle.builder()
				.text("Title H4")
				.level(4)
				.build();
		final AntDesignTitle titleH5 = AntDesignTitle.builder()
				.text("Title H5")
				.level(5)
				.build();
		titleShowcase.add(titleDefault, titleH1, titleH2, titleH3, titleH4, titleH5);
		add(new Label("Typography Title Showcase"));
		add(titleShowcase);
	}
}
