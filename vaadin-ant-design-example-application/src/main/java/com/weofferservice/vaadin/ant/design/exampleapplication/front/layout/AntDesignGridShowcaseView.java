package com.weofferservice.vaadin.ant.design.exampleapplication.front.layout;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.layout.grid.col.AntDesignCol;
import com.weofferservice.vaadin.ant.design.components.layout.grid.row.AntDesignRow;
import com.weofferservice.vaadin.ant.design.components.layout.grid.row.AntDesignRowAlign;
import com.weofferservice.vaadin.ant.design.components.layout.grid.row.AntDesignRowJustify;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Peaceful Coder
 */
@Route("grid")
public class AntDesignGridShowcaseView extends VerticalLayout {

	public AntDesignGridShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasicGrid();
		addGridHorizontalGutter();
		addGridVerticalGutter();
		addGridVerticalAndHorizontalGutter();
		addGridColumnOffset();
		addGridSort();
		addGridTypesetting();
		addGridAlignment();
		addGridOrder();
		addGridOrderResponsive();
		addGridFlexStretchPercentageColumns();
		addGridFlexStretchFillRest();
		addGridFlexStretchRawFlexStyle();
		addGridFlexStretchRawFlexNone();
	}

	private void addGridFlexStretchRawFlexNone() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol colNone = AntDesignCol.builder()
				.flex("none")
				.tag(Tag.builder()
						.className("gutter")
						.text("none")
						.build(TagName.DIV))
				.build();
		final AntDesignCol colAuto = AntDesignCol.builder()
				.flex("auto")
				.tag(Tag.builder()
						.className("gutter")
						.text("auto")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(colNone)
						.addColumn(colAuto)
						.build();
		showcase.add(row);
		add(new Label("Grid Flex Stretch (Flex none)"));
		add(showcase);
	}

	private void addGridFlexStretchRawFlexStyle() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col1_1_200px = AntDesignCol.builder()
				.flex("1 1 200px")
				.tag(Tag.builder()
						.className("gutter")
						.text("1 1 200px")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col0_1_300px = AntDesignCol.builder()
				.flex("0 1 300px")
				.tag(Tag.builder()
						.className("gutter")
						.text("0 1 300px")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col1_1_200px)
						.addColumn(col0_1_300px)
						.build();
		showcase.add(row);
		add(new Label("Grid Flex Stretch (Raw flex style)"));
		add(showcase);
	}

	private void addGridFlexStretchFillRest() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col100px = AntDesignCol.builder()
				.flex("100px")
				.tag(Tag.builder()
						.className("gutter")
						.text("100px")
						.build(TagName.DIV))
				.build();
		final AntDesignCol colAuto = AntDesignCol.builder()
				.flex("auto")
				.tag(Tag.builder()
						.className("gutter")
						.text("auto")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col100px)
						.addColumn(colAuto)
						.build();
		showcase.add(row);
		add(new Label("Grid Flex Stretch (Fill rest)"));
		add(showcase);
	}

	private void addGridFlexStretchPercentageColumns() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col2_5 = AntDesignCol.builder()
				.flex(2)
				.tag(Tag.builder()
						.className("gutter")
						.text("2 / 5")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col3_5 = AntDesignCol.builder()
				.flex(3)
				.tag(Tag.builder()
						.className("gutter")
						.text("3 / 5")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col2_5)
						.addColumn(col3_5)
						.build();
		showcase.add(row);
		add(new Label("Grid Flex Stretch (Percentage columns)"));
		add(showcase);
	}

	private void addGridOrderResponsive() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col6_1 = AntDesignCol.builder()
				.span(6)
				.xsOrder(1).smOrder(2).mdOrder(3).lgOrder(4)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 1")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_2 = AntDesignCol.builder()
				.span(6)
				.xsOrder(2).smOrder(1).mdOrder(4).lgOrder(3)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 2")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_3 = AntDesignCol.builder()
				.span(6)
				.xsOrder(3).smOrder(4).mdOrder(2).lgOrder(1)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 3")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_4 = AntDesignCol.builder()
				.span(6)
				.xsOrder(4).smOrder(3).mdOrder(1).lgOrder(2)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 4")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col6_1)
						.addColumn(col6_2)
						.addColumn(col6_3)
						.addColumn(col6_4)
						.build();
		showcase.add(row);
		add(new Label("Grid Order Responsive"));
		add(showcase);
	}

	private void addGridOrder() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col6_1to3 = AntDesignCol.builder()
				.span(6)
				.order(3)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 1to3")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_2to1 = AntDesignCol.builder()
				.span(6)
				.order(1)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 2to1")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_3to4 = AntDesignCol.builder()
				.span(6)
				.order(4)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 3to4")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_4to2 = AntDesignCol.builder()
				.span(6)
				.order(2)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 4to2")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col6_1to3)
						.addColumn(col6_2to1)
						.addColumn(col6_3to4)
						.addColumn(col6_4to2)
						.build();
		showcase.add(row);
		add(new Label("Grid Order"));
		add(showcase);
	}

	private void addGridAlignment() {
		final AntDesignCol col4_100px = AntDesignCol.builder()
				.span(4)
				.tag(Tag.builder()
						.className("gutter")
						.style(Collections.singletonMap("height", "100px"))
						.text("col-4")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col4_50px = AntDesignCol.builder()
				.span(4)
				.tag(Tag.builder()
						.className("gutter")
						.style(Collections.singletonMap("height", "50px"))
						.text("col-4")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col4_120px = AntDesignCol.builder()
				.span(4)
				.tag(Tag.builder()
						.className("gutter")
						.style(Collections.singletonMap("height", "120px"))
						.text("col-4")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col4_80px = AntDesignCol.builder()
				.span(4)
				.tag(Tag.builder()
						.className("gutter")
						.style(Collections.singletonMap("height", "80px"))
						.text("col-4")
						.build(TagName.DIV))
				.build();
		Arrays.stream(AntDesignRowAlign.values()).forEach(
				antDesignRowAlign -> {
					final FlexLayout showcase = new FlexLayout();
					showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
					showcase.setWidthFull();
					final AntDesignRow row =
							AntDesignRow.builder()
									.addColumn(col4_100px)
									.addColumn(col4_50px)
									.addColumn(col4_120px)
									.addColumn(col4_80px)
									.align(antDesignRowAlign)
									.justify(AntDesignRowJustify.CENTER)
									.build();
					showcase.add(row);
					add(new Label("Grid Alignment - " + antDesignRowAlign));
					add(showcase);
				}
		);
	}

	private void addGridTypesetting() {
		final AntDesignCol.Builder col4 = AntDesignCol.builder()
				.span(4)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-4")
						.build(TagName.DIV));
		Arrays.stream(AntDesignRowJustify.values()).forEach(
				antDesignRowJustify -> {
					final FlexLayout showcase = new FlexLayout();
					showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
					showcase.setWidthFull();
					final AntDesignRow row =
							AntDesignRow.builder()
									.addColumn(col4.build())
									.addColumn(col4.build())
									.addColumn(col4.build())
									.addColumn(col4.build())
									.justify(antDesignRowJustify)
									.build();
					showcase.add(row);
					add(new Label("Grid Typesetting - " + antDesignRowJustify));
					add(showcase);
				}
		);
	}

	private void addGridSort() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col18_push6 = AntDesignCol.builder()
				.span(18)
				.push(6)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-18 push-6")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col6_pull18 = AntDesignCol.builder()
				.span(6)
				.pull(18)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 pull-18")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col18_push6)
						.addColumn(col6_pull18)
						.build();
		showcase.add(row);
		add(new Label("Grid Sort"));
		add(showcase);
	}

	private void addGridColumnOffset() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol col6_offset3 = AntDesignCol.builder()
				.span(6)
				.offset(3)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6 offset-3")
						.build(TagName.DIV))
				.build();
		final AntDesignCol col12_offset3 = AntDesignCol.builder()
				.span(12)
				.offset(3)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-12 offset-3")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row =
				AntDesignRow.builder()
						.addColumn(col6_offset3)
						.addColumn(col12_offset3)
						.build();
		showcase.add(row);
		add(new Label("Grid Column Offset"));
		add(showcase);
	}

	private void addGridVerticalAndHorizontalGutter() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol.Builder col6Builder = AntDesignCol.builder()
				.span(6)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6")
						.build(TagName.DIV));
		final AntDesignRow.Builder rowBuilder = AntDesignRow.builder()
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.horizontalGutter(8, 16, 24, 32)
				.verticalGutter(8, 16, 24, 32);
		showcase.add(rowBuilder.build());
		showcase.add(rowBuilder.build());
		add(new Label("Grid Vertical And Horizontal Gutter"));
		add(showcase);
	}

	private void addGridVerticalGutter() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol.Builder col6Builder = AntDesignCol.builder()
				.span(6)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6")
						.build(TagName.DIV));
		final AntDesignRow.Builder rowBuilder = AntDesignRow.builder()
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.verticalGutter(8, 16, 24, 32);
		showcase.add(rowBuilder.build());
		showcase.add(rowBuilder.build());
		add(new Label("Grid Vertical Gutter"));
		add(showcase);
	}

	private void addGridHorizontalGutter() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignCol.Builder col6Builder = AntDesignCol.builder()
				.span(6)
				.tag(Tag.builder()
						.className("gutter")
						.text("col-6")
						.build(TagName.DIV));
		final AntDesignRow.Builder rowBuilder = AntDesignRow.builder()
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.horizontalGutter(8, 16, 24, 32);
		showcase.add(rowBuilder.build());
		showcase.add(rowBuilder.build());
		add(new Label("Grid Horizontal Gutter"));
		add(showcase);
	}

	private void addBasicGrid() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		//Row 1
		final AntDesignCol col24 = AntDesignCol.builder()
				.span(24)
				.className("gutter")
				.tag(Tag.builder()
						.text("col-24")
						.build(TagName.DIV))
				.build();
		final AntDesignRow row1 = AntDesignRow.builder()
				.addColumn(col24)
				.build();
		showcase.add(row1);
		//Row 2
		final AntDesignCol.Builder col12Builder = AntDesignCol.builder()
				.span(12)
				.className("gutter")
				.tag(Tag.builder()
						.text("col-12")
						.build(TagName.DIV));
		final AntDesignRow row2 = AntDesignRow.builder()
				.addColumn(col12Builder.build())
				.addColumn(col12Builder.build())
				.build();
		showcase.add(row2);
		//Row 3
		final AntDesignCol.Builder col8Builder = AntDesignCol.builder()
				.span(8)
				.className("gutter")
				.tag(Tag.builder()
						.text("col-8")
						.build(TagName.DIV));
		final AntDesignRow row3 = AntDesignRow.builder()
				.addColumn(col8Builder.build())
				.addColumn(col8Builder.build())
				.addColumn(col8Builder.build())
				.build();
		showcase.add(row3);
		//Row 4
		final AntDesignCol.Builder col6Builder = AntDesignCol.builder()
				.span(6)
				.className("gutter")
				.tag(Tag.builder()
						.text("col-6")
						.build(TagName.DIV));
		final AntDesignRow row4 = AntDesignRow.builder()
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.addColumn(col6Builder.build())
				.build();
		showcase.add(row4);
		add(new Label("Basic Grid"));
		add(showcase);
	}
}
