package com.weofferservice.vaadin.ant.design.exampleapplication.front.layout;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnCollapseEvent;
import com.weofferservice.vaadin.ant.design.components.layout.layout.AntDesignLayout;
import com.weofferservice.vaadin.ant.design.components.layout.layout.AntDesignLayoutType;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

/**
 * @author Peaceful Coder
 */
@Route("collapsible-layout")
public class AntDesignCollapsibleLayoutShowcaseView extends VerticalLayout {

	public AntDesignCollapsibleLayoutShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignLayout sider = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.SIDER)
				.collapsible(true)
				.turnOnCollapse()
				.addChild(Tag.builder()
						.className("gutter")
						.text("Sider")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout header = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.HEADER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Header")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout content = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.CONTENT)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Content")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout footer = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.FOOTER)
				.addChild(Tag.builder()
						.className("gutter")
						.text("Footer")
						.build(TagName.DIV))
				.build();
		final AntDesignLayout basicStructure = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(header)
				.addChild(content)
				.addChild(footer)
				.build();
		final AntDesignLayout layout = AntDesignLayout.builder()
				.layoutType(AntDesignLayoutType.LAYOUT)
				.addChild(sider)
				.addChild(basicStructure)
				.build();
		layout.addListener(AntDesignOnCollapseEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"collapsed = " + event.isCollapsed() + "\");");
		});
		showcase.add(layout);
		add(showcase);
	}
}
