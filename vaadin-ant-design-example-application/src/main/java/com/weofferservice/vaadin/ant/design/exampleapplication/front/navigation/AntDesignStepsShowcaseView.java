package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.AntDesignDirectionType;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.navigation.AntDesignSize;
import com.weofferservice.vaadin.ant.design.components.navigation.steps.AntDesignStep;
import com.weofferservice.vaadin.ant.design.components.navigation.steps.AntDesignStepStatus;
import com.weofferservice.vaadin.ant.design.components.navigation.steps.AntDesignSteps;
import com.weofferservice.vaadin.ant.design.components.navigation.steps.AntDesignStepsType;

/**
 * @author Peaceful Coder
 */
@Route("steps")
public class AntDesignStepsShowcaseView extends VerticalLayout {

	public AntDesignStepsShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasic();
		addMiniVersion();
		addWithIcon();
		addVertical();
		addNavigationSteps();
		addStepsWithProgress();
	}

	private void addStepsWithProgress() {
		final String title = "Steps with progress";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Finished")
				.description("This is a description.")
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("In Progress")
				.subTitle("Left 00:00:08")
				.description("This is a description.")
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Waiting")
				.description("This is a description.")
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.current(1)
				.percent(60)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}

	private void addNavigationSteps() {
		final String title = "Navigation Steps";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Finished")
				.description("This is a description.")
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("In Progress")
				.subTitle("Left 00:00:08")
				.description("This is a description.")
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Waiting")
				.description("This is a description.")
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.current(1)
				.type(AntDesignStepsType.NAVIGATION)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}

	private void addVertical() {
		final String title = "Vertical";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Finished")
				.description("This is a description.")
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("In Progress")
				.subTitle("Left 00:00:08")
				.description("This is a description.")
//				.status(AntDesignStepStatus.ERROR)
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Waiting")
				.description("This is a description.")
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.current(1)
				.direction(AntDesignDirectionType.VERTICAL)
				.progressDot(true)
//				.size(AntDesignSize.SMALL)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}

	private void addWithIcon() {
		final String title = "With Icon";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Login")
				.status(AntDesignStepStatus.FINISH)
				.icon(AntDesignIcon.builder()
						.iconType(AntDesignIconType.USER_OUTLINED)
						.rotate(45)
						.build())
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("Verification")
				.status(AntDesignStepStatus.FINISH)
				.icon(AntDesignIcon.builder()
						.iconType(AntDesignIconType.SOLUTION_OUTLINED)
						.spin(true)
						.build())
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Pay")
				.status(AntDesignStepStatus.PROCESS)
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.LOADING_OUTLINED).build())
				.build();
		final AntDesignStep step4 = AntDesignStep.builder()
				.title("Done")
				.status(AntDesignStepStatus.WAIT)
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.SMILE_OUTLINED).build())
				.build();
		final AntDesignStep step5 = AntDesignStep.builder()
				.title("Super done")
				.status(AntDesignStepStatus.ERROR)
				.icon(AntDesignIcon.builder()
						.iconType(AntDesignIconType.ICON)
						.svgComponentName("HeartSvg")
						.className("pinkAndBig")
						.spin(true)
						.build())
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.progressDot(true)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.addChild(step4)
				.addChild(step5)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}

	private void addMiniVersion() {
		final String title = "Mini Version";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Finished")
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("In Progress")
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Waiting")
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.current(1)
				.size(AntDesignSize.SMALL)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}

	private void addBasic() {
		final String title = "Basic";
		final FlexLayout showcase = new FlexLayout();
		showcase.setWidthFull();
		final AntDesignStep step1 = AntDesignStep.builder()
				.title("Finished")
				.description("This is a description.")
				.build();
		final AntDesignStep step2 = AntDesignStep.builder()
				.title("In Progress")
				.subTitle("Left 00:00:08")
				.description("This is a description.")
				.build();
		final AntDesignStep step3 = AntDesignStep.builder()
				.title("Waiting")
				.description("This is a description.")
				.build();
		final AntDesignSteps steps = AntDesignSteps.builder()
				.current(1)
				.progressDot(true)
				.addChild(step1)
				.addChild(step2)
				.addChild(step3)
				.build();
		steps.setWidthFull();
		showcase.add(steps);
		add(new Label(title));
		add(showcase);
	}
}
