package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;

import java.util.Arrays;

/**
 * @author Peaceful Coder
 */
@Route("icons")
public class AntDesignIconsShowcaseView extends VerticalLayout {

	public AntDesignIconsShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Default icons
		final FlexLayout iconsShowcase = new FlexLayout();
		iconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignIconType.values()).forEach(
				antDesignIconType -> {
					if (antDesignIconType == AntDesignIconType.ICON) {
						return;
					}
					final AntDesignIcon icon = AntDesignIcon.builder()
							.iconType(antDesignIconType)
							.build();
					icon.getStyle().set("margin", "5px");
					iconsShowcase.add(icon);
				}
		);
		add(new Label("Default icons"));
		add(iconsShowcase);
		//Rotate (45 degrees) icons
		final FlexLayout rotateIconsShowcase = new FlexLayout();
		rotateIconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignIconType.values()).forEach(
				antDesignIconType -> {
					if (antDesignIconType == AntDesignIconType.ICON) {
						return;
					}
					final AntDesignIcon icon = AntDesignIcon.builder()
							.iconType(antDesignIconType)
							.rotate(45) // 45 degrees
							.build();
					icon.getStyle().set("margin", "5px");
					rotateIconsShowcase.add(icon);
				}
		);
		add(new Label("Rotate (45 degrees) icons"));
		add(rotateIconsShowcase);
		//Spin and TwoToneColor icons
		final FlexLayout spinAndTwoToneColorIconsShowcase = new FlexLayout();
		spinAndTwoToneColorIconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignIconType.values()).forEach(
				antDesignIconType -> {
					if (antDesignIconType == AntDesignIconType.ICON) {
						return;
					}
					final AntDesignIcon icon = AntDesignIcon.builder()
							.iconType(antDesignIconType)
							.spin(true)
							.twoToneColor("#eb2f96")
							.build();
					icon.getStyle().set("margin", "5px");
					spinAndTwoToneColorIconsShowcase.add(icon);
				}
		);
		add(new Label("Spin and TwoToneColor icons"));
		add(spinAndTwoToneColorIconsShowcase);
		//Custom spin icons
		final FlexLayout customSpinIconsShowcase = new FlexLayout();
		customSpinIconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.asList("PandaSvg", "HeartSvg").forEach(
				svgComponentName -> {
					final AntDesignIcon icon = AntDesignIcon.builder()
							.svgComponentName(svgComponentName)
							.spin(true)
							.build();
					icon.getStyle().set("margin", "5px");
					customSpinIconsShowcase.add(icon);
				}
		);
		add(new Label("Custom spin icons"));
		add(customSpinIconsShowcase);
		//Custom icons with custom className
		final FlexLayout customIconsWithCustomClassNameShowcase = new FlexLayout();
		customIconsWithCustomClassNameShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.asList("PandaSvg", "HeartSvg").forEach(
				svgComponentName -> {
					final AntDesignIcon icon = AntDesignIcon.builder()
							.svgComponentName(svgComponentName)
							.className("pinkAndBig")
							.build();
					icon.getStyle().set("margin", "5px");
					customIconsWithCustomClassNameShowcase.add(icon);
				}
		);
		add(new Label("Custom icons with custom className"));
		add(customIconsWithCustomClassNameShowcase);
	}
}
