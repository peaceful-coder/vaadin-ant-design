package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.general.typography.link.AntDesignLink;
import com.weofferservice.vaadin.ant.design.components.general.typography.text.AntDesignText;
import com.weofferservice.vaadin.ant.design.components.general.typography.text.AntDesignTextType;

/**
 * @author Peaceful Coder
 */
@Route("typography-text-and-link")
public class AntDesignTypographyTextShowcaseView extends VerticalLayout {

	public AntDesignTypographyTextShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Typography Text And Link Showcase
		final FlexLayout textShowcase = new FlexLayout();
		textShowcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		final AntDesignText textDefault = AntDesignText.builder()
				.text("Text Default")
				.build();
		final AntDesignText textSecondary = AntDesignText.builder()
				.text("Text type = SECONDARY")
				.type(AntDesignTextType.SECONDARY)
				.build();
		final AntDesignText textDanger = AntDesignText.builder()
				.text("Text type = DANGER")
				.type(AntDesignTextType.DANGER)
				.build();
		final AntDesignText textSuccess = AntDesignText.builder()
				.text("Text type = SUCCESS")
				.type(AntDesignTextType.SUCCESS)
				.build();
		final AntDesignText textWarning = AntDesignText.builder()
				.text("Text type = WARNING")
				.type(AntDesignTextType.WARNING)
				.build();
		final AntDesignText textDisabled = AntDesignText.builder()
				.text("Text Disabled")
				.disabled(true)
				.build();
		final AntDesignText textMark = AntDesignText.builder()
				.text("Text Mark")
				.mark(true)
				.build();
		final AntDesignText textCode = AntDesignText.builder()
				.text("Text Code")
				.code(true)
				.build();
		final AntDesignText textKeyboard = AntDesignText.builder()
				.text("Text Keyboard")
				.keyboard(true)
				.build();
		final AntDesignText textUnderline = AntDesignText.builder()
				.text("Text Underline")
				.underline(true)
				.build();
		final AntDesignText textDelete = AntDesignText.builder()
				.text("Text Delete")
				.delete(true)
				.build();
		final AntDesignText textStrong = AntDesignText.builder()
				.text("Text Strong")
				.strong(true)
				.build();
		final AntDesignLink link = AntDesignLink.builder()
				.text("Link")
				.target("_blank")
				.href("https://google.com")
				.build();
		textShowcase.add(
				textDefault, textSecondary, textDanger, textSuccess, textWarning,
				textDisabled, textMark, textCode, textKeyboard, textUnderline, textDelete, textStrong,
				link);
		add(new Label("Typography Text And Link Showcase"));
		add(textShowcase);
	}
}
