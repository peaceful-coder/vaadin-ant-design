package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnTitleClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButton;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenu;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuItem;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuItemGroup;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuMode;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuTheme;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignSubMenu;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignSubMenuProps;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Collections;

/**
 * @author Peaceful Coder
 */
@Route("menus")
public class AntDesignMenuShowcaseView extends VerticalLayout {

	public AntDesignMenuShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addSimpleMenu();
		addTopNavigation();
		addInlineMenu();
		addCollapsedInlineMenu();
		addOpenCurrentSubmenuOnly();
		addVerticalMenu();
	}

	private void addVerticalMenu() {
		final FlexLayout showcase = new FlexLayout();
		//SubMenu #1
		final AntDesignMenuItem subMenu1Item1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 1").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1Item2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 2").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenu1ItemGroup1 = AntDesignMenuItemGroup.builder()
				.title("Item 1")
				.addMenuItem(subMenu1Item1)
				.addMenuItem(subMenu1Item2)
				.build();
		final AntDesignMenuItem subMenu1Item3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 3").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1Item4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 4").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenu1ItemGroup2 = AntDesignMenuItemGroup.builder()
				.title("Item 2")
				.addMenuItem(subMenu1Item3)
				.addMenuItem(subMenu1Item4)
				.build();
		final AntDesignSubMenu subMenu1 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.MAIL_OUTLINED).build())
				.title("Navigation One")
				.addChild(subMenu1ItemGroup1)
				.addChild(subMenu1ItemGroup2)
				.build();
		//SubMenu #2
		final AntDesignMenuItem subMenu2MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 7").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu2MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 8").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu2 = AntDesignSubMenu.builder()
				.title("Submenu")
				.addChild(subMenu2MenuItem1)
				.addChild(subMenu2MenuItem2)
				.build();
		//SubMenu #3
		final AntDesignMenuItem subMenu3MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 5").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu3MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 6").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu3 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.APPSTORE_OUTLINED).build())
				.title("Navigation Two")
				.addChild(subMenu3MenuItem1)
				.addChild(subMenu3MenuItem2)
				.addChild(subMenu2)
				.build();
		//SubMenu #4
		final AntDesignMenuItem subMenu4MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 9").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 10").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 11").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 12").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu4 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.SETTING_OUTLINED).build())
				.title("Navigation Three")
				.addChild(subMenu4MenuItem1)
				.addChild(subMenu4MenuItem2)
				.addChild(subMenu4MenuItem3)
				.addChild(subMenu4MenuItem4)
				.build();
		//Menu
		final AntDesignMenu menu = AntDesignMenu.builder()
				.mode(AntDesignMenuMode.VERTICAL)
				.style(Collections.singletonMap("width", "256px"))
				.addChild(subMenu1)
				.addChild(subMenu3)
				.addChild(subMenu4)
				.build();
		showcase.add(menu);
		add(new Label("Vertical menu"));
		add(showcase);
	}

	private void addOpenCurrentSubmenuOnly() {
		final FlexLayout showcase = new FlexLayout();
		//SubMenu #1
		final AntDesignMenuItem subMenu1MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 1").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 2").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 3").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 4").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu1 = AntDesignSubMenu.builder()
				.title("Navigation One")
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.MAIL_OUTLINED).build())
				.addChild(subMenu1MenuItem1)
				.addChild(subMenu1MenuItem2)
				.addChild(subMenu1MenuItem3)
				.addChild(subMenu1MenuItem4)
				.build();
		//SubMenu #3
		final AntDesignMenuItem subMenu3MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 7").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu3MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 8").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu3 = AntDesignSubMenu.builder()
				.title("Submenu")
				.addChild(subMenu3MenuItem1)
				.addChild(subMenu3MenuItem2)
				.build();
		//SubMenu #2
		final AntDesignMenuItem subMenu2MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 5").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu2MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 6").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu2 = AntDesignSubMenu.builder()
				.title("Navigation Two")
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.APPSTORE_OUTLINED).build())
				.addChild(subMenu2MenuItem1)
				.addChild(subMenu2MenuItem2)
				.addChild(subMenu3)
				.build();
		//SubMenu #4
		final AntDesignMenuItem subMenu4MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 9").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 10").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 11").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 12").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu4 = AntDesignSubMenu.builder()
				.title("Navigation Three")
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.SETTING_OUTLINED).build())
				.addChild(subMenu4MenuItem1)
				.addChild(subMenu4MenuItem2)
				.addChild(subMenu4MenuItem3)
				.addChild(subMenu4MenuItem4)
				.build();
		//Menu
		final AntDesignMenu menu = AntDesignMenu.builder()
				.mode(AntDesignMenuMode.INLINE)
				.addChild(subMenu1)
				.addChild(subMenu2)
				.addChild(subMenu4)
				.markAsOpen(subMenu2)
				.markAsOpen(subMenu3)
				.build();
		showcase.add(menu);
		add(new Label("Open current submenu only"));
		add(showcase);
	}

	private void addCollapsedInlineMenu() {
		final FlexLayout showcase = new FlexLayout();
		//Button
		final AntDesignButton collapseButton = AntDesignButton.builder()
				.buttonType(AntDesignButtonType.PRIMARY)
				.style(Collections.singletonMap("marginBottom", "16px"))
				.children(AntDesignIcon.builder().iconType(AntDesignIconType.MENU_UNFOLD_OUTLINED).build())
				.build();
		//Items
		final AntDesignMenuItem item1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 1").build(TagName.SPAN))
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.PIE_CHART_OUTLINED).build())
				.build();
		final AntDesignMenuItem item2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 2").build(TagName.SPAN))
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.DESKTOP_OUTLINED).build())
				.build();
		final AntDesignMenuItem item3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 3").build(TagName.SPAN))
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.CONTAINER_OUTLINED).build())
				.build();
		//SubMenu #1
		final AntDesignMenuItem subMenu1MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 5").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 6").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 7").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1MenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 8").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu1 = AntDesignSubMenu.builder()
				.title("Navigation One")
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.MAIL_OUTLINED).build())
				.addChild(subMenu1MenuItem1)
				.addChild(subMenu1MenuItem2)
				.addChild(subMenu1MenuItem3)
				.addChild(subMenu1MenuItem4)
				.build();
		//SubMenu #3
		final AntDesignMenuItem subMenu3MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 11").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu3MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 12").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu3 = AntDesignSubMenu.builder()
				.title("Submenu")
				.addChild(subMenu3MenuItem1)
				.addChild(subMenu3MenuItem2)
				.build();
		//SubMenu #2
		final AntDesignMenuItem subMenu2MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 9").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu2MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 10").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu2 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.APPSTORE_OUTLINED).build())
				.title("Navigation Two")
				.addChild(subMenu2MenuItem1)
				.addChild(subMenu2MenuItem2)
				.addChild(subMenu3)
				.build();
		//Menu
		final AntDesignMenu menu = AntDesignMenu.builder()
				.mode(AntDesignMenuMode.INLINE)
				.theme(AntDesignMenuTheme.DARK)
				.inlineCollapsed(false)
				.addChild(item1)
				.addChild(item2)
				.addChild(item3)
				.addChild(subMenu1)
				.addChild(subMenu2)
				.markAsDefaultSelected(item1)
				.markAsDefaultOpen(subMenu1)
				.markAsSelected(item1)
				.build();
		//Div
		final Tag div =
				Tag.builder()
						.addChild(collapseButton)
						.addChild(menu)
						.build(TagName.DIV);
		showcase.add(div);
		add(new Label("Collapsed Inline Menu"));
		add(showcase);
	}

	private void addInlineMenu() {
		final FlexLayout showcase = new FlexLayout();
		//SubMenu #1
		final AntDesignMenuItem subMenu1Item1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 1").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1Item2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 2").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenu1ItemGroup1 = AntDesignMenuItemGroup.builder()
				.title("Item 1")
				.addMenuItem(subMenu1Item1)
				.addMenuItem(subMenu1Item2)
				.build();
		final AntDesignMenuItem subMenu1Item3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 3").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu1Item4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 4").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenu1ItemGroup2 = AntDesignMenuItemGroup.builder()
				.title("Item 2")
				.addMenuItem(subMenu1Item3)
				.addMenuItem(subMenu1Item4)
				.build();
		final AntDesignSubMenu subMenu1 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.MAIL_OUTLINED).build())
				.title("Navigation One")
				.addChild(subMenu1ItemGroup1)
				.addChild(subMenu1ItemGroup2)
				.turnOnTitleClick()
				.build();
		//SubMenu #2
		final AntDesignMenuItem subMenu2MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 7").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu2MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 8").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu2 = AntDesignSubMenu.builder()
				.title("Submenu")
				.addChild(subMenu2MenuItem1)
				.addChild(subMenu2MenuItem2)
				.turnOnTitleClick()
				.build();
		//SubMenu #3
		final AntDesignMenuItem subMenu3MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 5").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu3MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 6").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu3 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.APPSTORE_OUTLINED).build())
				.title("Navigation Two")
				.addChild(subMenu3MenuItem1)
				.addChild(subMenu3MenuItem2)
				.addChild(subMenu2)
				.turnOnTitleClick()
				.build();
		//SubMenu #4
		final AntDesignMenuItem subMenu4MenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 9").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 10").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 11").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenu4MenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 12").build(TagName.SPAN))
				.build();
		final AntDesignSubMenu subMenu4 = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.SETTING_OUTLINED).build())
				.title("Navigation Three")
				.addChild(subMenu4MenuItem1)
				.addChild(subMenu4MenuItem2)
				.addChild(subMenu4MenuItem3)
				.addChild(subMenu4MenuItem4)
				.turnOnTitleClick()
				.build();
		//Menu
		final AntDesignMenu menu = AntDesignMenu.builder()
				.mode(AntDesignMenuMode.INLINE)
				.style(Collections.singletonMap("width", "256px"))
				.addChild(subMenu1)
				.addChild(subMenu3)
				.addChild(subMenu4)
				.markAsDefaultSelected(subMenu1Item1)
				.markAsDefaultOpen(subMenu1)
				.markAsSelected(subMenu1Item1)
				.turnOnClick()
				.build();
		menu.addListener(AntDesignOnTitleClickEvent.class, event -> {
			if (subMenu1.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"subMenu title clicked = "
								+ ((AntDesignSubMenuProps) subMenu1.getComponent().getProps()).getTitle()
								+ "\");");
			} else if (subMenu2.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"subMenu title clicked = "
								+ ((AntDesignSubMenuProps) subMenu2.getComponent().getProps()).getTitle()
								+ "\");");
			} else if (subMenu3.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"subMenu title clicked = "
								+ ((AntDesignSubMenuProps) subMenu3.getComponent().getProps()).getTitle()
								+ "\");");
			} else if (subMenu4.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"subMenu title clicked = "
								+ ((AntDesignSubMenuProps) subMenu4.getComponent().getProps()).getTitle()
								+ "\");");
			}
		});
		menu.addListener(AntDesignOnClickEvent.class, event -> {
			if (subMenu1Item1.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu1Item1.getTitle()
								+ "\");");
			} else if (subMenu1Item2.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu1Item2.getTitle()
								+ "\");");
			} else if (subMenu1Item3.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu1Item3.getTitle()
								+ "\");");
			} else if (subMenu1Item4.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu1Item4.getTitle()
								+ "\");");
			} else if (subMenu2MenuItem1.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu2MenuItem1.getTitle()
								+ "\");");
			} else if (subMenu2MenuItem2.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu2MenuItem2.getTitle()
								+ "\");");
			} else if (subMenu3MenuItem1.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu3MenuItem1.getTitle()
								+ "\");");
			} else if (subMenu3MenuItem2.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu3MenuItem2.getTitle()
								+ "\");");
			} else if (subMenu4MenuItem1.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu4MenuItem1.getTitle()
								+ "\");");
			} else if (subMenu4MenuItem2.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu4MenuItem2.getTitle()
								+ "\");");
			} else if (subMenu4MenuItem3.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu4MenuItem3.getTitle()
								+ "\");");
			} else if (subMenu4MenuItem4.getKey().equals(event.getKey())) {
				UI.getCurrent().getPage().executeJs(
						"console.log(\"menu item clicked = "
								+ subMenu4MenuItem4.getTitle()
								+ "\");");
			}
		});
		showcase.add(menu);
		add(new Label("Inline Menu"));
		add(showcase);
	}

	private void addTopNavigation() {
		final FlexLayout showcase = new FlexLayout();
		final AntDesignMenuItem subMenuItem1 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 1").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenuItem2 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 2").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenuItemGroup1 = AntDesignMenuItemGroup.builder()
				.title("Item 1")
				.addMenuItem(subMenuItem1)
				.addMenuItem(subMenuItem2)
				.build();
		final AntDesignMenuItem subMenuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 3").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem subMenuItem4 = AntDesignMenuItem.builder()
				.tag(Tag.builder().text("Option 4").build(TagName.SPAN))
				.build();
		final AntDesignMenuItemGroup subMenuItemGroup2 = AntDesignMenuItemGroup.builder()
				.title("Item 2")
				.addMenuItem(subMenuItem3)
				.addMenuItem(subMenuItem4)
				.build();
		final AntDesignSubMenu subMenu = AntDesignSubMenu.builder()
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.SETTING_OUTLINED).build())
				.title("Navigation Three - Submenu")
				.addChild(subMenuItemGroup1)
				.addChild(subMenuItemGroup2)
				.build();
		final AntDesignMenuItem menuItem1 = AntDesignMenuItem.builder()
				.title("Navigation One - Title")
				.tag(Tag.builder().text("Navigation One").build(TagName.SPAN))
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.MAIL_OUTLINED).build())
				.build();
		final AntDesignMenuItem menuItem2 = AntDesignMenuItem.builder()
				.title("Navigation Two - Title")
				.tag(Tag.builder().text("Navigation Two").build(TagName.SPAN))
				.icon(AntDesignIcon.builder().iconType(AntDesignIconType.APPSTORE_OUTLINED).build())
				.disabled(true)
				.build();
		final AntDesignMenuItem menuItem3 = AntDesignMenuItem.builder()
				.tag(Tag.builder()
						.text("Navigation Four - Link")
						.setAttribute("href", "https://google.com")
						.setAttribute("target", "_blank")
						.build(TagName.A))
				.build();
		final AntDesignMenu menu = AntDesignMenu.builder()
				.mode(AntDesignMenuMode.HORIZONTAL)
				.addChild(menuItem1).markAsSelected(menuItem1)
				.addChild(menuItem2)
				.addChild(subMenu)
				.addChild(menuItem3)
				.build();
		showcase.add(menu);
		add(new Label("Top Navigation"));
		add(showcase);
	}

	private void addSimpleMenu() {
		final FlexLayout simpleMenuShowcase = new FlexLayout();
		final AntDesignMenuItem menuItem1 = AntDesignMenuItem.builder()
				.title("item 1 title")
				.tag(Tag.builder().text("item 1 text").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem menuItem2 = AntDesignMenuItem.builder()
				.title("item 2 title")
				.tag(Tag.builder().text("item 2 text").build(TagName.SPAN))
				.danger(true)
				.build();
		final AntDesignMenu menu = AntDesignMenu.builder()
				.addChild(menuItem1)
				.addChild(menuItem2)
				.build();
		simpleMenuShowcase.add(menu);
		add(new Label("Simple menu"));
		add(simpleMenuShowcase);
	}
}
