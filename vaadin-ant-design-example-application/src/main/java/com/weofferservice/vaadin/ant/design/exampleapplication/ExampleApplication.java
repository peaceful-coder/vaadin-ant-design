package com.weofferservice.vaadin.ant.design.exampleapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Peaceful Coder
 */
@SpringBootApplication
public class ExampleApplication {

	public static void main(String... args) {
		SpringApplication.run(ExampleApplication.class);
	}
}