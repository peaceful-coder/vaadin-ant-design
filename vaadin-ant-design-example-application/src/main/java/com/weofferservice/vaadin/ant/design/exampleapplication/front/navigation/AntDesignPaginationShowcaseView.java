package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.navigation.AntDesignSize;
import com.weofferservice.vaadin.ant.design.components.navigation.pagination.AntDesignPagination;

/**
 * @author Peaceful Coder
 */
@Route("pagination")
public class AntDesignPaginationShowcaseView extends VerticalLayout {

	public AntDesignPaginationShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasic();
		addMore();
		addShowSizeChangerAndDisabled();
		addShowQuickJumper();
		addMiniSize();
		addSimpleMode();
		addControlled();
		addDefaultPageSize();
	}

	private void addDefaultPageSize() {
		final String title = "Default Page Size";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.defaultPageSize(15)
				.total(85)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addControlled() {
		final String title = "Controlled";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.current(2)
				.total(50)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addSimpleMode() {
		final String title = "Simple Mode";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.simple(true)
				.defaultCurrent(2)
				.total(50)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addMiniSize() {
		final String title = "Mini Size";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.size(AntDesignSize.SMALL)
				.defaultCurrent(2)
				.total(500)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addShowQuickJumper() {
		final String title = "Show Quick Jumper";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.showQuickJumper(true)
				.defaultCurrent(2)
				.total(500)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addShowSizeChangerAndDisabled() {
		final String title = "Show Size Changer and Disabled";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.showSizeChanger(false)
				.disabled(true)
				.defaultCurrent(3)
				.total(500)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addMore() {
		final String title = "More";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.defaultCurrent(6)
				.total(500)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}

	private void addBasic() {
		final String title = "Basic";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPagination pagination = AntDesignPagination.builder()
				.defaultCurrent(1)
				.total(50)
				.build();
		showcase.add(pagination);
		add(new Label(title));
		add(showcase);
	}
}
