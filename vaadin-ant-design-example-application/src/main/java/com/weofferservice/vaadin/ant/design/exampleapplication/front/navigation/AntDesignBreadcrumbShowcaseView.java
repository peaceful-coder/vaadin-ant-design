package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb.AntDesignBreadcrumb;
import com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb.AntDesignBreadcrumbItem;
import com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb.AntDesignBreadcrumbSeparator;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenu;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuItem;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@Route("breadcrumb")
public class AntDesignBreadcrumbShowcaseView extends VerticalLayout {

	public AntDesignBreadcrumbShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasicUsage();
		addWithAnIcon();
		addConfiguringTheSeparator1();
		addConfiguringTheSeparator2();
		addDropDownMenu();
	}

	private void addDropDownMenu() {
		add(new Label("Drop Down Menu"));
		//Menu
		final AntDesignMenuItem menuItem1 = AntDesignMenuItem.builder()
				.title("item 1 title")
				.tag(Tag.builder().text("item 1 text").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem menuItem2 = AntDesignMenuItem.builder()
				.title("item 2 title")
				.tag(Tag.builder().text("item 2 text").build(TagName.SPAN))
				.danger(true)
				.build();
		final AntDesignMenu menu = AntDesignMenu.builder()
				.addChild(menuItem1)
				.addChild(menuItem2)
				.build();
		//Showcase
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignBreadcrumbItem breadcrumbItem1 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Ant Design")
								.build(TagName.SPAN))
				.turnOnClick()
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem2 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Component")
								.setAttribute("href", "")
								.build(TagName.A))
				.turnOnClick()
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem3 = AntDesignBreadcrumbItem.builder()
				.menu(menu)
				.addChild(
						Tag.builder()
								.text("General")
								.setAttribute("href", "")
								.build(TagName.A))
				.turnOnClick()
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem4 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Button")
								.build(TagName.SPAN))
				.turnOnClick()
				.build();
		final AntDesignBreadcrumb breadcrumb = AntDesignBreadcrumb.builder()
				.addChild(breadcrumbItem1)
				.addChild(breadcrumbItem2)
				.addChild(breadcrumbItem3)
				.addChild(breadcrumbItem4)
				.build();
		breadcrumb.addListener(AntDesignOnClickEvent.class, event -> {
			final UUID antDesignBaseComponentUuid = event.getAntDesignBaseComponentUuid();
			if (breadcrumbItem1.getUuid().equals(antDesignBaseComponentUuid)) {
				UI.getCurrent().getPage().executeJs("alert(\"breadcrumbItem1\");");
			} else if (breadcrumbItem2.getUuid().equals(antDesignBaseComponentUuid)) {
				UI.getCurrent().getPage().executeJs("alert(\"breadcrumbItem2\");");
			} else if (breadcrumbItem3.getUuid().equals(antDesignBaseComponentUuid)) {
				UI.getCurrent().getPage().executeJs("alert(\"breadcrumbItem3\");");
			} else if (breadcrumbItem4.getUuid().equals(antDesignBaseComponentUuid)) {
				UI.getCurrent().getPage().executeJs("alert(\"breadcrumbItem4\");");
			}
		});
		showcase.add(breadcrumb);
		add(showcase);
	}

	private void addConfiguringTheSeparator2() {
		add(new Label("Configuring the Separator 2"));
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignBreadcrumbSeparator separator1 = AntDesignBreadcrumbSeparator.builder()
				.addChild(
						Tag.builder()
								.text(":")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbSeparator.Builder separator2Builder = AntDesignBreadcrumbSeparator.builder();
		final AntDesignBreadcrumbItem breadcrumbItem1 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Location")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem2 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						Tag.builder()
								.text("Application Center")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem3 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						Tag.builder()
								.text("Application List")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem4 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("An Application")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumb breadcrumb = AntDesignBreadcrumb.builder()
				.separator("")
				.addChild(breadcrumbItem1)
				.addChild(separator1)
				.addChild(breadcrumbItem2)
				.addChild(separator2Builder.build())
				.addChild(breadcrumbItem3)
				.addChild(separator2Builder.build())
				.addChild(breadcrumbItem4)
				.build();
		showcase.add(breadcrumb);
		add(showcase);
	}

	private void addConfiguringTheSeparator1() {
		add(new Label("Configuring the Separator 1"));
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignBreadcrumbItem breadcrumbItem1 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Home")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem2 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						Tag.builder()
								.text("Application Center")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem3 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						Tag.builder()
								.text("Application List")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem4 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("An Application")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumb breadcrumb = AntDesignBreadcrumb.builder()
				.separator(">")
				.addChild(breadcrumbItem1)
				.addChild(breadcrumbItem2)
				.addChild(breadcrumbItem3)
				.addChild(breadcrumbItem4)
				.build();
		showcase.add(breadcrumb);
		add(showcase);
	}

	private void addWithAnIcon() {
		add(new Label("With an Icon"));
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignBreadcrumbItem breadcrumbItem1 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						AntDesignIcon.builder()
								.iconType(AntDesignIconType.HOME_OUTLINED)
								.build())
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem2 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						AntDesignIcon.builder()
								.iconType(AntDesignIconType.USER_OUTLINED)
								.build())
				.addChild(
						Tag.builder()
								.text("Application List")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem3 = AntDesignBreadcrumbItem.builder()
				.href("")
				.addChild(
						AntDesignIcon.builder()
								.spin(true)
								.svgComponentName("HeartSvg")
								.className("pinkAndBig")
								.build())
				.addChild(
						Tag.builder()
								.text("Application Center")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem4 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Application")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumb breadcrumb = AntDesignBreadcrumb.builder()
				.addChild(breadcrumbItem1)
				.addChild(breadcrumbItem2)
				.addChild(breadcrumbItem3)
				.addChild(breadcrumbItem4)
				.build();
		showcase.add(breadcrumb);
		add(showcase);
	}

	private void addBasicUsage() {
		add(new Label("Basic Usage"));
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		showcase.setWidthFull();
		final AntDesignBreadcrumbItem breadcrumbItem1 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Home")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem2 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Application Center")
								.setAttribute("href", "")
								.build(TagName.A))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem3 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("Application List")
								.setAttribute("href", "")
								.build(TagName.A))
				.build();
		final AntDesignBreadcrumbItem breadcrumbItem4 = AntDesignBreadcrumbItem.builder()
				.addChild(
						Tag.builder()
								.text("An Application")
								.build(TagName.SPAN))
				.build();
		final AntDesignBreadcrumb breadcrumb = AntDesignBreadcrumb.builder()
				.addChild(breadcrumbItem1)
				.addChild(breadcrumbItem2)
				.addChild(breadcrumbItem3)
				.addChild(breadcrumbItem4)
				.build();
		showcase.add(breadcrumb);
		add(showcase);
	}
}
