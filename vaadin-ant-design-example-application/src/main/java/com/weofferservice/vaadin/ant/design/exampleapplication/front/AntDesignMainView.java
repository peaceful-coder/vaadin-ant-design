package com.weofferservice.vaadin.ant.design.exampleapplication.front;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.OrderedList;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteData;
import com.vaadin.flow.router.Router;
import com.weofferservice.vaadin.ant.design.components.general.typography.link.AntDesignLink;
import com.weofferservice.vaadin.ant.design.components.general.typography.title.AntDesignTitle;
import lombok.RequiredArgsConstructor;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author Peaceful Coder
 */
@Route(AntDesignMainView.MAIN_ROUTE)
@CssImport("./style.css")
@RequiredArgsConstructor
public class AntDesignMainView extends VerticalLayout {

	static final String MAIN_ROUTE = "";

	private final UI ui;

	@PostConstruct
	public void init() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		add(AntDesignTitle.builder()
				.text("Vaadin Ant Design Example Application")
				.build());
		final OrderedList orderedList = new OrderedList();
		add(orderedList);
		final Router router = ui.getRouter();
		final List<RouteData> routes = router.getRegistry().getRegisteredRoutes();
		routes.forEach(routeData -> {
			final String url = routeData.getUrl();
			if (!MAIN_ROUTE.equals(url)) {
				orderedList.add(new ListItem(
						AntDesignLink.builder()
								.text(url)
								.href(url)
								.target("_blank")
								.build()
				));
			}
		});
	}
}
