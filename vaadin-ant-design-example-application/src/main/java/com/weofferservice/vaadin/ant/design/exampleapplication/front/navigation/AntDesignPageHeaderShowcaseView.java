package com.weofferservice.vaadin.ant.design.exampleapplication.front.navigation;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnBackEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButton;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.navigation.pageheader.AntDesignPageHeader;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Collections;
import java.util.Map;

/**
 * @author Peaceful Coder
 */
@Route("page-header")
public class AntDesignPageHeaderShowcaseView extends VerticalLayout {

	public AntDesignPageHeaderShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addBasicPageHeader();
		addWhiteBackgroundMode();
		addUseWithBreadcrumbs();
		addUseTagsAndAvatar();
		addUseFooter();
	}

	private void addUseFooter() {
		final String title = "Use Footer";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPageHeader pageHeader = AntDesignPageHeader.builder()
				.title("Title")
				.subTitle("This is a subtitle")
				.style(Collections.singletonMap("border", "1px solid rgb(235, 237, 240)"))
				.footer(Tag.builder()
						.style(Map.of(
								"color", "green",
								"padding", "2px",
								"margin", "1px",
								"border", "1px solid green"))
						.text("Footer")
						.build(TagName.SPAN))
				.turnOnBack()
				.build();
		pageHeader.addListener(AntDesignOnBackEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		showcase.add(pageHeader);
		add(new Label(title));
		add(showcase);
	}

	private void addUseTagsAndAvatar() {
		final String title = "Use Tags and Avatar";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPageHeader pageHeader = AntDesignPageHeader.builder()
				.title("Title")
				.subTitle("This is a subtitle")
				.style(Collections.singletonMap("border", "1px solid rgb(235, 237, 240)"))
				.avatar("https://avatars1.githubusercontent.com/u/8186664?s=460&v=4")
				.addTag(Tag.builder()
						.style(Map.of(
								"color", "blue",
								"padding", "2px",
								"margin", "1px",
								"border", "1px solid blue"))
						.text("Running 1")
						.build(TagName.SPAN))
				.addTag(Tag.builder()
						.style(Map.of(
								"color", "green",
								"padding", "2px",
								"margin", "1px",
								"border", "1px solid green"))
						.text("Running 2")
						.build(TagName.SPAN))
				.turnOnBack()
				.build();
		pageHeader.addListener(AntDesignOnBackEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		showcase.add(pageHeader);
		add(new Label(title));
		add(showcase);
	}

	private void addUseWithBreadcrumbs() {
		final String title = "Use with breadcrumbs";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPageHeader pageHeader = AntDesignPageHeader.builder()
				.title("Title")
				.subTitle("This is a subtitle")
				.style(Collections.singletonMap("border", "1px solid rgb(235, 237, 240)"))
				.addBreadcrumb("index", "First-level Menu")
				.addBreadcrumb("first", "Second-level Menu")
				.addBreadcrumb("second", "Third-level Menu")
				.turnOnBack()
				.build();
		pageHeader.addListener(AntDesignOnBackEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		showcase.add(pageHeader);
		add(new Label(title));
		add(showcase);
	}

	private void addWhiteBackgroundMode() {
		final String title = "White Background Mode";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPageHeader pageHeader = AntDesignPageHeader.builder()
				.ghost(false)
				.title("Title")
				.subTitle("This is a subtitle")
				.addChild(
						AntDesignButton.builder()
								.children(Tag.builder().text("Operation 1").build(TagName.SPAN))
								.build())
				.addChild(
						AntDesignButton.builder()
								.children(Tag.builder().text("Operation 2").build(TagName.SPAN))
								.build())
				.addChild(
						AntDesignButton.builder()
								.buttonType(AntDesignButtonType.PRIMARY)
								.children(Tag.builder().text("Primary").build(TagName.SPAN))
								.build())
				.turnOnBack()
				.build();
		//Div
		final Tag div =
				Tag.builder()
						.addChild(pageHeader)
						.style(Map.of(
								"backgroundColor", "#f5f5f5",
								"padding", "24px"
						))
						.build(TagName.DIV);
		div.addListener(AntDesignOnBackEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
//			UI.getCurrent().getPage().executeJs("window.history.back();");
		});
		showcase.add(div);
		add(new Label(title));
		add(showcase);
	}

	private void addBasicPageHeader() {
		final String title = "Basic Page Header";
		final FlexLayout showcase = new FlexLayout();
		final AntDesignPageHeader pageHeader = AntDesignPageHeader.builder()
				.title("Title")
				.subTitle("This is a subtitle")
				.style(Collections.singletonMap("border", "1px solid rgb(235, 237, 240)"))
				.turnOnBack()
				.build();
		pageHeader.addListener(AntDesignOnBackEvent.class, event -> {
			UI.getCurrent().getPage().executeJs("alert(\"" + title + "\");");
		});
		showcase.add(pageHeader);
		add(new Label(title));
		add(showcase);
	}
}
