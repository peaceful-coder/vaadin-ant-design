package com.weofferservice.vaadin.ant.design.exampleapplication.front.layout;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.AntDesignDirectionType;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButton;
import com.weofferservice.vaadin.ant.design.components.layout.space.AntDesignAlign;
import com.weofferservice.vaadin.ant.design.components.layout.space.AntDesignSizeType;
import com.weofferservice.vaadin.ant.design.components.layout.space.AntDesignSpace;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Collections;

/**
 * @author Peaceful Coder
 */
@Route("space")
public class AntDesignSpaceShowcaseView extends VerticalLayout {

	public AntDesignSpaceShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addHorizontalSpace();
		addVerticalSpace();
		addAlignSpace();
		addSplitSpace();
	}

	private void addSplitSpace() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.ROW);
		showcase.setWidthFull();
		final AntDesignButton.Builder buttonBuilder = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN));
		final AntDesignSpace space = AntDesignSpace.builder()
				.split(Tag.builder()
						.className("gutter")
						.text("Split")
						.build(TagName.DIV))
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.build();
		showcase.add(space);
		add(new Label("Split Space"));
		add(showcase);
	}

	private void addAlignSpace() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.ROW);
		showcase.setWidthFull();
		final AntDesignButton.Builder buttonBuilder = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN));
		final AntDesignSpace space = AntDesignSpace.builder()
				.align(AntDesignAlign.CENTER)
				.addChild(buttonBuilder.style(Collections.singletonMap("height", "50px")).build())
				.addChild(buttonBuilder.style(Collections.singletonMap("height", "150px")).build())
				.addChild(buttonBuilder.style(Collections.singletonMap("height", "100px")).build())
				.build();
		showcase.add(space);
		add(new Label("Align Space"));
		add(showcase);
	}

	private void addVerticalSpace() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.ROW);
		showcase.setWidthFull();
		final AntDesignButton.Builder buttonBuilder = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN));
		final AntDesignSpace space = AntDesignSpace.builder()
				.size(AntDesignSizeType.LARGE)
				.direction(AntDesignDirectionType.VERTICAL)
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.build();
		showcase.add(space);
		add(new Label("Vertical Space"));
		add(showcase);
	}

	private void addHorizontalSpace() {
		final FlexLayout showcase = new FlexLayout();
		showcase.setFlexDirection(FlexLayout.FlexDirection.ROW);
		showcase.setWidthFull();
		final AntDesignButton.Builder buttonBuilder = AntDesignButton.builder()
				.children(Tag.builder().text("Button").build(TagName.SPAN));
		final AntDesignSpace space = AntDesignSpace.builder()
				.size(AntDesignSizeType.LARGE)
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.addChild(buttonBuilder.build())
				.build();
		showcase.add(space);
		add(new Label("Horizontal Space"));
		add(showcase);
	}
}
