package com.weofferservice.vaadin.ant.design.exampleapplication.front.layout;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.layout.divider.AntDesignDivider;
import com.weofferservice.vaadin.ant.design.components.layout.divider.AntDesignDividerOrientation;
import com.weofferservice.vaadin.ant.design.components.layout.divider.AntDesignDividerType;

/**
 * @author Peaceful Coder
 */
@Route("divider")
public class AntDesignDividerShowcaseView extends VerticalLayout {

	public AntDesignDividerShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		addHorizontalDividers();
		addVerticalDividers();
	}

	private void addHorizontalDividers() {
		//Horizontal Dividers
		final FlexLayout horizontalDividerShowcase = new FlexLayout();
		horizontalDividerShowcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		horizontalDividerShowcase.setWidthFull();
		//Left Horizontal Dashed
		final AntDesignDivider leftHorizontalDashedDivider = AntDesignDivider.builder()
				.text("Left Horizontal Dashed")
				.orientation(AntDesignDividerOrientation.LEFT)
				.type(AntDesignDividerType.HORIZONTAL)
				.dashed(true)
				.build();
		horizontalDividerShowcase.add(leftHorizontalDashedDivider);
		horizontalDividerShowcase.add(new Label("Some text"));
		//Right Horizontal Plain
		final AntDesignDivider rightHorizontalPlainDivider = AntDesignDivider.builder()
				.text("Right Horizontal Plain")
				.orientation(AntDesignDividerOrientation.RIGHT)
				.type(AntDesignDividerType.HORIZONTAL)
				.plain(true)
				.build();
		horizontalDividerShowcase.add(rightHorizontalPlainDivider);
		horizontalDividerShowcase.add(new Label("Some text"));
		//Center Horizontal
		final AntDesignDivider centerHorizontalDivider = AntDesignDivider.builder()
				.text("Center Horizontal")
				.orientation(AntDesignDividerOrientation.CENTER)
				.type(AntDesignDividerType.HORIZONTAL)
				.build();
		horizontalDividerShowcase.add(centerHorizontalDivider);
		horizontalDividerShowcase.add(new Label("Some text"));
		add(new Label("Horizontal Dividers"));
		add(horizontalDividerShowcase);
	}

	private void addVerticalDividers() {
		//Vertical Dividers
		final FlexLayout verticalDividerShowcase = new FlexLayout();
		verticalDividerShowcase.setFlexDirection(FlexLayout.FlexDirection.ROW);
		verticalDividerShowcase.setWidthFull();
		final AntDesignDivider verticalDivider1 = AntDesignDivider.builder()
				.type(AntDesignDividerType.VERTICAL)
				.build();
		verticalDividerShowcase.add(verticalDivider1);
		verticalDividerShowcase.add(new Label("Some text"));
		final AntDesignDivider verticalDivider2 = AntDesignDivider.builder()
				.type(AntDesignDividerType.VERTICAL)
				.build();
		verticalDividerShowcase.add(verticalDivider2);
		verticalDividerShowcase.add(new Label("Some text"));
		final AntDesignDivider verticalDivider3 = AntDesignDivider.builder()
				.type(AntDesignDividerType.VERTICAL)
				.build();
		verticalDividerShowcase.add(verticalDivider3);
		verticalDividerShowcase.add(new Label("Some text"));
		add(new Label("Vertical Dividers"));
		add(verticalDividerShowcase);
	}
}
