package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButton;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonShape;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonSizeType;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Arrays;

/**
 * @author Peaceful Coder
 */
@Route("buttons")
public class AntDesignButtonsShowcaseView extends VerticalLayout {

	public AntDesignButtonsShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Types
		final FlexLayout buttonTypesShowcase = new FlexLayout();
		buttonTypesShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.turnOnClick(
									JsFunction.builder()
											.argumentNames(Arrays.asList(
													"x", "y"
											))
											.bodyLines(Arrays.asList(
													"console.log(\"buttonCaption = \" + \"" + antDesignButtonType.name() + "\");",
													"alert(\"" + antDesignButtonType.name() + "\");"
											))
											.build())
							.build();
					button.getStyle().set("margin", "5px");
					buttonTypesShowcase.add(button);
				}
		);
		add(new Label("Types"));
		add(buttonTypesShowcase);
		//Shapes
		final FlexLayout buttonShapesShowcase = new FlexLayout();
		buttonShapesShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonShape.values()).forEach(
				antDesignButtonShape -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonShape.name()).build(TagName.SPAN))
							.buttonShape(antDesignButtonShape)
							.turnOnClick()
							.build();
					button.addListener(AntDesignOnClickEvent.class, event -> {
						UI.getCurrent().getPage().executeJs("alert(\"" + antDesignButtonShape.name() + "\");");
					});
					button.getStyle().set("margin", "5px");
					buttonShapesShowcase.add(button);
				}
		);
		add(new Label("Shapes"));
		add(buttonShapesShowcase);
		//Icons
		final AntDesignIcon icon1 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.STAR_TWO_TONE)
				.twoToneColor("green")
				.spin(true)
				.build();
		final AntDesignIcon icon2 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.ICON)
				.svgComponentName("HeartSvg")
				.className("pinkAndBig")
				.spin(true)
				.build();
		final FlexLayout buttonIconsShowcase = new FlexLayout();
		buttonIconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.asList(icon1, icon2).forEach(
				antDesignIcon -> {
					final AntDesignButton button = AntDesignButton.builder()
							.icon(antDesignIcon)
							.children(Tag.builder().text("Press me").build(TagName.SPAN))
							.build();
					button.getStyle().set("margin", "5px");
					buttonIconsShowcase.add(button);
				}
		);
		add(new Label("Icons"));
		add(buttonIconsShowcase);
		//Size types
		final FlexLayout buttonSizeTypesShowcase = new FlexLayout();
		buttonSizeTypesShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonSizeType.values()).forEach(
				antDesignButtonSizeType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.size(antDesignButtonSizeType)
							.children(Tag.builder().text(antDesignButtonSizeType.name()).build(TagName.SPAN))
							.buttonShape(AntDesignButtonShape.ROUND)
							.build();
					button.getStyle().set("margin", "5px");
					buttonSizeTypesShowcase.add(button);
				}
		);
		add(new Label("Size types"));
		add(buttonSizeTypesShowcase);
		//Disabled
		final FlexLayout buttonDisabledShowcase = new FlexLayout();
		buttonDisabledShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.buttonShape(AntDesignButtonShape.CIRCLE)
							.disabled(true)
							.build();
					button.getStyle().set("margin", "5px");
					buttonDisabledShowcase.add(button);
				}
		);
		add(new Label("Disabled"));
		add(buttonDisabledShowcase);
		//Loading
		final FlexLayout buttonLoadingShowcase = new FlexLayout();
		buttonLoadingShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.loading(true)
							.build();
					button.getStyle().set("margin", "5px");
					buttonLoadingShowcase.add(button);
				}
		);
		add(new Label("Loading"));
		add(buttonLoadingShowcase);
		//Ghost
		final FlexLayout buttonGhostShowcase = new FlexLayout();
		buttonGhostShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.ghost(true)
							.build();
					button.getStyle().set("margin", "5px");
					buttonGhostShowcase.add(button);
				}
		);
		add(new Label("Ghost"));
		add(buttonGhostShowcase);
		//Danger
		final FlexLayout buttonDangerShowcase = new FlexLayout();
		buttonDangerShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.danger(true)
							.build();
					button.getStyle().set("margin", "5px");
					buttonDangerShowcase.add(button);
				}
		);
		add(new Label("Danger"));
		add(buttonDangerShowcase);
		//Block
		final FlexLayout buttonBlockShowcase = new FlexLayout();
		buttonBlockShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignButton button = AntDesignButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.block(true)
							.build();
					button.getStyle().set("margin", "5px");
					buttonBlockShowcase.add(button);
				}
		);
		add(new Label("Block"));
		add(buttonBlockShowcase);
	}
}
