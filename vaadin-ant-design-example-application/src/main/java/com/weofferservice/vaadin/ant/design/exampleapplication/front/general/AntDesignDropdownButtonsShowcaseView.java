package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnVisibleChangeEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonSizeType;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.general.button.dropdown.AntDesignDropdownButton;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenu;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenuItem;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;

import java.util.Arrays;

/**
 * @author Peaceful Coder
 */
@Route("dropdown-buttons")
public class AntDesignDropdownButtonsShowcaseView extends VerticalLayout {

	public AntDesignDropdownButtonsShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Menu for all showcases
		final AntDesignMenuItem menuItem1 = AntDesignMenuItem.builder()
				.title("item 1 title")
				.tag(Tag.builder().text("item 1 text").build(TagName.SPAN))
				.build();
		final AntDesignMenuItem menuItem2 = AntDesignMenuItem.builder()
				.title("item 2 title")
				.tag(Tag.builder().text("item 2 text").build(TagName.SPAN))
				.danger(true)
				.build();
		final AntDesignMenu menu = AntDesignMenu.builder()
				.addChild(menuItem1)
				.addChild(menuItem2)
				.build();
		//Types
		final FlexLayout buttonTypesShowcase = new FlexLayout();
		buttonTypesShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignDropdownButton button = AntDesignDropdownButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.menu(menu)
							.turnOnClick()
							.turnOnVisibleChange()
							.build();
					button.addListener(AntDesignOnClickEvent.class, event -> {
						UI.getCurrent().getPage().executeJs(
								"alert(\"" + antDesignButtonType.name() + " was clicked\");");
					});
					button.addListener(AntDesignOnVisibleChangeEvent.class, event -> {
						UI.getCurrent().getPage().executeJs(
								"alert(\"" + antDesignButtonType.name() + " visible was changed on " + event.isVisible() + "\");");
					});
					button.getStyle().set("margin", "5px");
					buttonTypesShowcase.add(button);
				}
		);
		add(new Label("Types"));
		add(buttonTypesShowcase);
		//Icons
		final AntDesignIcon icon1 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.STAR_TWO_TONE)
				.twoToneColor("green")
				.spin(true)
				.build();
		final AntDesignIcon icon2 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.ICON)
				.svgComponentName("HeartSvg")
				.className("pinkAndBig")
				.spin(true)
				.build();
		final FlexLayout buttonIconsShowcase = new FlexLayout();
		buttonIconsShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.asList(icon1, icon2).forEach(
				antDesignIcon -> {
					final AntDesignDropdownButton button = AntDesignDropdownButton.builder()
							.icon(antDesignIcon)
							.children(Tag.builder().text("Press me").build(TagName.SPAN))
							.menu(menu)
							.build();
					button.getStyle().set("margin", "5px");
					buttonIconsShowcase.add(button);
				}
		);
		add(new Label("Icons"));
		add(buttonIconsShowcase);
		//Size types
		final FlexLayout buttonSizeTypesShowcase = new FlexLayout();
		buttonSizeTypesShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonSizeType.values()).forEach(
				antDesignButtonSizeType -> {
					final AntDesignDropdownButton button = AntDesignDropdownButton.builder()
							.size(antDesignButtonSizeType)
							.children(Tag.builder().text(antDesignButtonSizeType.name()).build(TagName.SPAN))
							.menu(menu)
							.build();
					button.getStyle().set("margin", "5px");
					buttonSizeTypesShowcase.add(button);
				}
		);
		add(new Label("Size types"));
		add(buttonSizeTypesShowcase);
		//Disabled
		final FlexLayout buttonDisabledShowcase = new FlexLayout();
		buttonDisabledShowcase.setFlexWrap(FlexLayout.FlexWrap.WRAP);
		Arrays.stream(AntDesignButtonType.values()).forEach(
				antDesignButtonType -> {
					final AntDesignDropdownButton button = AntDesignDropdownButton.builder()
							.children(Tag.builder().text(antDesignButtonType.name()).build(TagName.SPAN))
							.buttonType(antDesignButtonType)
							.disabled(true)
							.menu(menu)
							.build();
					button.getStyle().set("margin", "5px");
					buttonDisabledShowcase.add(button);
				}
		);
		add(new Label("Disabled"));
		add(buttonDisabledShowcase);
	}
}
