package com.weofferservice.vaadin.ant.design.exampleapplication.front.general;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIconType;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.copyable.AntDesignCopyableParagraph;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.editable.AntDesignEditableParagraph;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.ellipsis.AntDesignEllipsisParagraph;

/**
 * @author Peaceful Coder
 */
@Route("typography-paragraph")
public class AntDesignTypographyParagraphShowcaseView extends VerticalLayout {

	public AntDesignTypographyParagraphShowcaseView() {
		setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		//Icons
		final AntDesignIcon icon1 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.STAR_TWO_TONE)
				.twoToneColor("green")
				.spin(true)
				.build();
		final AntDesignIcon icon2 = AntDesignIcon.builder()
				.iconType(AntDesignIconType.ICON)
				.svgComponentName("HeartSvg")
				.className("pinkAndBig")
				.spin(true)
				.build();
		//Typography Paragraph Showcase
		final FlexLayout paragraphShowcase = new FlexLayout();
		paragraphShowcase.setFlexDirection(FlexLayout.FlexDirection.COLUMN);
		final AntDesignEditableParagraph paragraphEditable = AntDesignEditableParagraph.builder()
				.text("Editable")
//				.icon(icon1)
				.tooltip("Edit me now!")
				.build();
		final AntDesignCopyableParagraph paragraphCopyable = AntDesignCopyableParagraph.builder()
				.text("Copyable")
//				.iconBefore(icon1)
//				.iconAfter(icon2)
				.tooltipBefore("Copy me now!")
				.tooltipAfter("You copied me already!")
				.build();
		final String text = "Ant Design, a design language for background applications, is refined by Ant UED Team. Ant " +
				"            Design, a design language for background applications, is refined by Ant UED Team. Ant Design, " +
				"            a design language for background applications, is refined by Ant UED Team. Ant Design, a " +
				"            design language for background applications, is refined by Ant UED Team. Ant Design, a design " +
				"            language for background applications, is refined by Ant UED Team. Ant Design, a design " +
				"            language for background applications, is refined by Ant UED Team.";
		final AntDesignEllipsisParagraph paragraphEllipsis = AntDesignEllipsisParagraph.builder()
				.text(text)
				.rows(2)
				.expandable(true)
				.symbol("more")
				.build();
		final AntDesignEllipsisParagraph paragraphEllipsisWithSuffix = AntDesignEllipsisParagraph.builder()
				.text(text)
				.rows(2)
				.expandable(true)
				.symbol("more")
				.suffix("<--William Shakespeare")
				.build();
		paragraphShowcase.add(paragraphEditable, paragraphCopyable, paragraphEllipsis, paragraphEllipsisWithSuffix);
		add(new Label("Typography Paragraph Showcase"));
		add(paragraphShowcase);
	}
}
