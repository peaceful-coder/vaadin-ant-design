//import origin
import * as svgs from './svg/module';
import * as antDesignCommonComponents from 'antd';
import * as antDesignLayouts from 'antd/lib/layout/layout';
import Icon, * as antDesignIcons from '@ant-design/icons';
import Title from 'antd/lib/typography/Title';
import Text from 'antd/lib/typography/Text';
import Link from 'antd/lib/typography/Link';
import Paragraph from 'antd/lib/typography/Paragraph';
import Sider from 'antd/lib/layout/Sider';
import MenuItem from 'antd/lib/menu/MenuItem';
import SubMenu from 'antd/lib/menu/SubMenu';
import MenuItemGroup from 'rc-menu/lib/MenuItemGroup';
import DropdownButton from 'antd/lib/dropdown/dropdown-button';
import BreadcrumbItem from 'antd/lib/breadcrumb/BreadcrumbItem';
import BreadcrumbSeparator from 'antd/lib/breadcrumb/BreadcrumbSeparator';
import Steps from 'antd/lib/steps';

//prepare data
const components = {};
for (const name in antDesignIcons) {
    components[name] = antDesignIcons[name];
}
for (const name in antDesignLayouts) {
    components[name] = antDesignLayouts[name];
}
for (const name in antDesignCommonComponents) {
    components[name] = antDesignCommonComponents[name];
}
for (const name in svgs) {
    components[name] = svgs[name];
}
components["Icon"] = Icon;
components["Title"] = Title;
components["Text"] = Text;
components["Link"] = Link;
components["Paragraph"] = Paragraph;
components["Sider"] = Sider;
components["MenuItem"] = MenuItem;
components["SubMenu"] = SubMenu;
components["MenuItemGroup"] = MenuItemGroup;
components["DropdownButton"] = DropdownButton;
components["BreadcrumbItem"] = BreadcrumbItem;
components["BreadcrumbSeparator"] = BreadcrumbSeparator;
components["Step"] = Steps.Step;

//export data
export default components;