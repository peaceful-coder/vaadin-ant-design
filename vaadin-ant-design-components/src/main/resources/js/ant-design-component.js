import React from 'react';
import ReactDOM from 'react-dom';
import components from './ant-design-component-import'

window.renderAntDesignComponent = (id, component) => {
    const antDesignComponent = window.createComponentTree(component);
    try {
        ReactDOM.render(
            antDesignComponent,
            document.getElementById(id)
        )
        window.fixStyles();
    } catch (exception) {
        if (exception.message !== "Target container is not a DOM element.") {
            throw exception;
        } else {
            //ignore
        }
    }
}

window.createComponentTree = (component) => {
    let reactComponent;
    const {javaClass, ...props} = window.constructProps(component.props);
    if (component.children === null || component.children === undefined) {
        reactComponent = React.createElement(
            resolveAntDesignBaseComponent(component.name),
            props);
    } else {
        const list = [];
        for (const childIndex in component.children) {
            const child = component.children[childIndex];
            list.push(window.createComponentTree(child));
        }
        reactComponent = React.createElement(
            resolveAntDesignBaseComponent(component.name),
            props,
            list);
    }
    return reactComponent;
}

window.constructProps = (props) => {
    for (const propName in props) {
        const propValue = props[propName];
        if (typeof propValue === 'object' && propValue !== null) {
            if (propValue.javaClass === ".ReactComponent") {
                props[propName] = window.createComponentTree(propValue);
            } else if (propValue.javaClass === ".JsFunction") {
                let args = "";
                {
                    const argumentNames = propValue.argumentNames;
                    for (const index in argumentNames) {
                        const argumentName = argumentNames[index];
                        if (args === "") {
                            args = argumentName;
                        } else {
                            args += ", " + argumentName;
                        }
                    }
                }
                let body = "";
                {
                    const bodyLines = propValue.bodyLines;
                    for (const index in bodyLines) {
                        const bodyLine = "\t" + bodyLines[index];
                        if (body === "") {
                            body = bodyLine;
                        } else {
                            body += "\n" + bodyLine;
                        }
                    }
                }
                const functionDescription = "(" + args + ") => {\n" + body + "\n}";
                console.log("functionDescription = " + functionDescription);
                props[propName] = eval(functionDescription);
            } else {
                props[propName] = window.constructProps(propValue);
            }
        }
    }
    return props;
}

window.resolveAntDesignBaseComponent = (componentName) => {
    let antDesignBaseComponent = components[componentName];
    if (antDesignBaseComponent === undefined) {
        antDesignBaseComponent = componentName;
    }
    return antDesignBaseComponent;
}

window.fixStyles = () => {
    const triggers = document.getElementsByClassName("ant-layout-sider-trigger");
    for (const index in triggers) {
        triggers[index].style.position = "relative";
    }
}
