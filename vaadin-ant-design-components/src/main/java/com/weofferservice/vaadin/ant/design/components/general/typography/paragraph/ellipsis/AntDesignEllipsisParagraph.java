package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.ellipsis;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.AntDesignParagraphProps;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignEllipsisParagraph extends AntDesignBaseComponent {

	private AntDesignEllipsisParagraph(@NonNull UUID antDesignBaseComponentUuid,
									   @NonNull AntDesignParagraphProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Paragraph")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private Integer rows = null;

		private Boolean expandable = null;

		private String symbol = null;

		private String suffix = null;

		private final AntDesignParagraphProps.AntDesignParagraphPropsBuilder propsBuilder = AntDesignParagraphProps.builder();

		public AntDesignEllipsisParagraph build() {
			propsBuilder
					.ellipsis(
							AntDesignEllipsisProps.builder()
									.rows(rows)
									.expandable(expandable)
									.symbol(symbol)
									.suffix(suffix)
									.build())
					.key(key);
			return new AntDesignEllipsisParagraph(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder rows(int rows) {
			this.rows = rows;
			return this;
		}

		public Builder expandable(boolean expandable) {
			this.expandable = expandable;
			return this;
		}

		public Builder symbol(@NonNull String symbol) {
			this.symbol = symbol;
			return this;
		}

		public Builder suffix(@NonNull String suffix) {
			this.suffix = suffix;
			return this;
		}
	}
}
