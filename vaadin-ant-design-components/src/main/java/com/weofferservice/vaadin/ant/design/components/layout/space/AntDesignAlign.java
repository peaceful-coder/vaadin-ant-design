package com.weofferservice.vaadin.ant.design.components.layout.space;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignAlign {
	START("start"),
	END("end"),
	CENTER("center"),
	BASELINE("baseline");

	@Getter
	private final String align;
}
