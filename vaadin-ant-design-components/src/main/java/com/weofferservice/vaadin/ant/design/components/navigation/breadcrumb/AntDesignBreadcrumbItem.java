package com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenu;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignBreadcrumbItem extends AntDesignBaseComponent {

	private AntDesignBreadcrumbItem(@NonNull UUID antDesignBaseComponentUuid,
									@NonNull AntDesignBreadcrumbItemProps antDesignBreadcrumbItemProps,
									@NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("BreadcrumbItem")
						.props(antDesignBreadcrumbItemProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignBreadcrumbItemProps.AntDesignBreadcrumbItemPropsBuilder propsBuilder =
				AntDesignBreadcrumbItemProps.builder();

		public AntDesignBreadcrumbItem build() {
			propsBuilder.key(key);
			return new AntDesignBreadcrumbItem(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignBaseComponent child) {
			children.add(child);
			return this;
		}

		public Builder turnOnClick() {
			final String id = AntDesignBreadcrumbItem.class.getSimpleName() + "-" + UUID.randomUUID();
			children.add( // technical tag for search by id
					Tag.builder()
							.setAttribute("id", id)
							.build(TagName.SPAN)
			);
			final JsFunction jsFunction = JsFunction.builder()
					.argumentNames(Collections.singletonList(
							"event"
					))
					.bodyLines(Arrays.asList(
							"event.preventDefault();",
							"const customEvent = new CustomEvent(\"" + AntDesignOnClickEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementById(\"" + id + "\")",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onClick(jsFunction);
			return this;
		}

		public Builder href(@NonNull String href) {
			propsBuilder.href(href);
			return this;
		}

		public Builder menu(@NonNull AntDesignMenu antDesignMenu) {
			propsBuilder.overlay(antDesignMenu.getComponent());
			return this;
		}
	}
}
