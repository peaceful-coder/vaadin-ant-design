package com.weofferservice.vaadin.ant.design.components.general.typography.title;

import com.weofferservice.vaadin.ant.design.components.general.typography.AntDesignBaseProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignTitleProps extends AntDesignBaseProps {

	Integer level;
}
