package com.weofferservice.vaadin.ant.design.components.layout.space;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignSizeType {
	SMALL("small"), //default
	MIDDLE("middle"),
	LARGE("large");

	@Getter
	private final String size;
}
