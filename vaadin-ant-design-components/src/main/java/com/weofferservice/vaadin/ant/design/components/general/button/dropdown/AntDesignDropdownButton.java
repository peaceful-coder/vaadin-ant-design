package com.weofferservice.vaadin.ant.design.components.general.button.dropdown;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnVisibleChangeEvent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonSizeType;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonType;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.navigation.menu.AntDesignMenu;
import lombok.NonNull;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignDropdownButton extends AntDesignBaseComponent {

	private AntDesignDropdownButton(@NonNull UUID antDesignBaseComponentUuid,
									@NonNull AntDesignDropdownButtonProps antDesignDropdownButtonProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("DropdownButton")
						.props(antDesignDropdownButtonProps)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private String className = null;

		private final AntDesignDropdownButtonProps.AntDesignDropdownButtonPropsBuilder propsBuilder =
				AntDesignDropdownButtonProps.builder();

		public AntDesignDropdownButton build() {
			propsBuilder
					.className(className)
					.key(key);
			return new AntDesignDropdownButton(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder turnOnClick() {
			final String classNameForReference = AntDesignDropdownButton.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnClickEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onClick(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder turnOnVisibleChange() {
			final String classNameForReference = AntDesignDropdownButton.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.argumentNames(Collections.singletonList(
							"visible"
					))
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnVisibleChangeEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"customEvent.visible = visible;",
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onVisibleChange(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder disabled(boolean disabled) {
			propsBuilder.disabled(disabled);
			return this;
		}

		public Builder size(@NonNull AntDesignButtonSizeType antDesignButtonSizeType) {
			propsBuilder.size(antDesignButtonSizeType.getSizeType());
			return this;
		}

		public Builder children(@NonNull AntDesignBaseComponent antDesignBaseComponent) {
			propsBuilder.children(antDesignBaseComponent.getComponent());
			return this;
		}

		public Builder buttonType(@NonNull AntDesignButtonType antDesignButtonType) {
			propsBuilder.type(antDesignButtonType.getType());
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon antDesignIcon) {
			propsBuilder.icon(antDesignIcon.getComponent());
			return this;
		}

		public Builder menu(@NonNull AntDesignMenu antDesignMenu) {
			propsBuilder.overlay(antDesignMenu.getComponent());
			return this;
		}

		public Builder className(@NonNull String className) {
			if (Objects.isNull(this.className)) {
				this.className = className;
			} else {
				this.className += " " + className;
			}
			return this;
		}
	}
}
