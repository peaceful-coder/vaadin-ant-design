package com.weofferservice.vaadin.ant.design.components.navigation.pageheader;

import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignRoute {

	String path;

	String breadcrumbName;
}
