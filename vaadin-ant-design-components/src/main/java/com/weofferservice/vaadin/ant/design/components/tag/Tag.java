package com.weofferservice.vaadin.ant.design.components.tag;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class Tag extends AntDesignBaseComponent {

	private Tag(@NonNull UUID antDesignBaseComponentUuid,
				@NonNull ReactComponent reactComponent) {
		super(
				antDesignBaseComponentUuid,
				reactComponent,
				true);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final Map<String, Object> attributes = new HashMap<>();

		private final TagProps.TagPropsBuilder propsBuilder = TagProps.builder();

		public Builder setAttribute(@NonNull String attributeName, @NonNull Object attributeValue) {
			attributes.put(attributeName, attributeValue);
			return this;
		}

		/**
		 * Adds child and cancels {@link Builder#text(String)}
		 */
		public Builder addChild(@NonNull AntDesignBaseComponent antDesignBaseComponent) {
			children.add(antDesignBaseComponent);
			propsBuilder.dangerouslySetInnerHTML(null);
			return this;
		}

		/**
		 * Adds text and cancels {@link Builder#addChild(AntDesignBaseComponent)}
		 */
		public Builder text(@NonNull String text) {
			propsBuilder.dangerouslySetInnerHTML(
					DangerouslySetInnerHTMLProps.builder().__html(text).build());
			children.clear();
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}

		public Tag build(@NonNull TagName tagName) {
			propsBuilder.key(key);
			return new Tag(
					antDesignBaseComponentUuid,
					ReactComponent.builder()
							.name(tagName.getName())
							.children(
									!children.isEmpty()
											? children.stream().map(AntDesignBaseComponent::getComponent).collect(toList())
											: null)
							.props(propsBuilder.attributes(attributes).build())
							.build()
			);
		}
	}
}
