package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.ellipsis;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignEllipsisProps extends ReactProps {

	Integer rows;

	Boolean expandable;

	String symbol;

	String suffix;
}
