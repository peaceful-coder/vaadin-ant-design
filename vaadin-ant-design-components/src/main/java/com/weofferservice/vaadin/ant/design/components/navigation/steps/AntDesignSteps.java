package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.AntDesignDirectionType;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.navigation.AntDesignSize;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignSteps extends AntDesignBaseComponent {

	private AntDesignSteps(@NonNull UUID antDesignBaseComponentUuid,
						   @NonNull AntDesignStepsProps antDesignStepsProps,
						   @NonNull List<AntDesignStep> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Steps")
						.props(antDesignStepsProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignStep> children = new ArrayList<>();

		private final AntDesignStepsProps.AntDesignStepsPropsBuilder propsBuilder = AntDesignStepsProps.builder();

		public AntDesignSteps build() {
			propsBuilder.key(key);
			return new AntDesignSteps(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignStep antDesignStep) {
			children.add(antDesignStep);
			return this;
		}

		public Builder current(int current) {
			propsBuilder.current(current);
			return this;
		}

		public Builder percent(int percent) {
			propsBuilder.percent(percent);
			return this;
		}

		public Builder progressDot(boolean progressDot) {
			propsBuilder.progressDot(progressDot);
			return this;
		}

		public Builder type(@NonNull AntDesignStepsType antDesignStepsType) {
			propsBuilder.type(antDesignStepsType.getType());
			return this;
		}

		public Builder direction(@NonNull AntDesignDirectionType antDesignDirectionType) {
			propsBuilder.direction(antDesignDirectionType.getDirection());
			return this;
		}

		public Builder size(@NonNull AntDesignSize antDesignSize) {
			propsBuilder.size(antDesignSize.getSize());
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}
	}
}
