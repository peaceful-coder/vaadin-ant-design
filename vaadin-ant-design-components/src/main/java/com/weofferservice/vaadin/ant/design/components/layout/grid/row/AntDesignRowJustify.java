package com.weofferservice.vaadin.ant.design.components.layout.grid.row;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignRowJustify {
	START("start"),
	END("end"),
	CENTER("center"),
	SPACE_AROUND("space-around"),
	SPACE_BETWEEN("space-between");

	@Getter
	private final String justify;
}
