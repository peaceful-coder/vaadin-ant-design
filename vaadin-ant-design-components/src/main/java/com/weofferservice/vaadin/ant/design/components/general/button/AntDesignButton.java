package com.weofferservice.vaadin.ant.design.components.general.button;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import lombok.NonNull;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignButton extends AntDesignBaseComponent {

	private AntDesignButton(@NonNull UUID antDesignBaseComponentUuid,
							@NonNull AntDesignButtonProps antDesignButtonProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Button")
						.props(antDesignButtonProps)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private String className = null;

		private boolean loading = false;

		private final AntDesignButtonProps.AntDesignButtonPropsBuilder propsBuilder = AntDesignButtonProps.builder();

		public AntDesignButton build() {
			propsBuilder
					.loading(loading)
					.className(className)
					.key(key);
			return new AntDesignButton(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder turnOnClick(@NonNull JsFunction jsFunction) {
			propsBuilder.onClick(jsFunction);
			return this;
		}

		public Builder turnOnClick() {
			final String classNameForReference = AntDesignButton.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnClickEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onClick(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder block(boolean block) {
			propsBuilder.block(block);
			return this;
		}

		public Builder danger(boolean danger) {
			propsBuilder.danger(danger);
			return this;
		}

		public Builder ghost(boolean ghost) {
			propsBuilder.ghost(ghost);
			return this;
		}

		public Builder loading(boolean loading) {
			this.loading = loading;
			return this;
		}

		public Builder disabled(boolean disabled) {
			propsBuilder.disabled(disabled);
			return this;
		}

		public Builder size(@NonNull AntDesignButtonSizeType antDesignButtonSizeType) {
			propsBuilder.size(antDesignButtonSizeType.getSizeType());
			return this;
		}

		public Builder children(@NonNull AntDesignBaseComponent antDesignBaseComponent) {
			propsBuilder.children(antDesignBaseComponent.getComponent());
			return this;
		}

		public Builder buttonType(@NonNull AntDesignButtonType antDesignButtonType) {
			propsBuilder.type(antDesignButtonType.getType());
			return this;
		}

		public Builder buttonShape(@NonNull AntDesignButtonShape antDesignButtonShape) {
			propsBuilder.shape(antDesignButtonShape.getShape());
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon antDesignIcon) {
			propsBuilder.icon(antDesignIcon.getComponent());
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}

		public Builder className(@NonNull String className) {
			if (Objects.isNull(this.className)) {
				this.className = className;
			} else {
				this.className += " " + className;
			}
			return this;
		}
	}
}
