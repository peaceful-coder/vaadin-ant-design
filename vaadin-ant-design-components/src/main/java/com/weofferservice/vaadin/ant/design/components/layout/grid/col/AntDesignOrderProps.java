package com.weofferservice.vaadin.ant.design.components.layout.grid.col;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignOrderProps extends ReactProps {

	Integer order;
}
