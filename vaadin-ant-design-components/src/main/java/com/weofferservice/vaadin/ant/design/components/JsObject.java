package com.weofferservice.vaadin.ant.design.components;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Peaceful Coder
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, property = "javaClass")
public interface JsObject {

}
