package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignMenuMode {
	HORIZONTAL("horizontal"),
	VERTICAL("vertical"),
	INLINE("inline");

	@Getter
	private final String mode;
}
