package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignMenuItemProps extends ReactProps {

	String title;

	Boolean danger;

	ReactComponent icon;

	Boolean disabled;
}
