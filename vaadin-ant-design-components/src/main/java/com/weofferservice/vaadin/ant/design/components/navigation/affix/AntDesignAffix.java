package com.weofferservice.vaadin.ant.design.components.navigation.affix;

import com.vaadin.flow.component.Component;
import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * Affix is very bugged element of Ant Design.
 *
 * @author Peaceful Coder
 */
public class AntDesignAffix extends AntDesignBaseComponent {

	private AntDesignAffix(@NonNull UUID antDesignBaseComponentUuid,
						   @NonNull AntDesignAffixProps antDesignAffixProps,
						   @NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Affix")
						.props(antDesignAffixProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignAffixProps.AntDesignAffixPropsBuilder propsBuilder = AntDesignAffixProps.builder();

		public AntDesignAffix build() {
			if (Objects.isNull(propsBuilder.build().getTarget())) {
				propsBuilder.target(
						JsFunction.builder()
								.bodyLines(Collections.singletonList(
										"return window;"
								))
								.build()
				);
			}
			propsBuilder.key(key);
			return new AntDesignAffix(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignBaseComponent child) {
			children.add(child);
			return this;
		}

		public Builder offsetTop(int offsetTop) {
			propsBuilder.offsetTop(offsetTop);
			return this;
		}

		public Builder offsetBottom(int offsetBottom) {
			propsBuilder.offsetBottom(offsetBottom);
			return this;
		}

		public Builder target(@NonNull Component component) {
			propsBuilder.target(
					JsFunction.builder()
							.bodyLines(Collections.singletonList(
									"return document.getElementById(\"" + component.getId().get() + "\");"
							))
							.build());
			return this;
		}
	}
}
