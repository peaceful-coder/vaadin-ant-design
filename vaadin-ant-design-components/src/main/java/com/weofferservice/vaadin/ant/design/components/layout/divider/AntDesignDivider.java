package com.weofferservice.vaadin.ant.design.components.layout.divider;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignDivider extends AntDesignBaseComponent {

	private AntDesignDivider(@NonNull UUID antDesignBaseComponentUuid,
							 @NonNull AntDesignDividerProps antDesignDividerProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Divider")
						.props(antDesignDividerProps)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignDividerProps.AntDesignDividerPropsBuilder propsBuilder = AntDesignDividerProps.builder();

		public AntDesignDivider build() {
			propsBuilder.key(key);
			return new AntDesignDivider(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder dashed(boolean dashed) {
			propsBuilder.dashed(dashed);
			return this;
		}

		public Builder plain(boolean plain) {
			propsBuilder.plain(plain);
			return this;
		}

		public Builder orientation(@NonNull AntDesignDividerOrientation antDesignDividerOrientation) {
			propsBuilder.orientation(antDesignDividerOrientation.getOrientation());
			return this;
		}

		public Builder type(@NonNull AntDesignDividerType antDesignDividerType) {
			propsBuilder.type(antDesignDividerType.getType());
			return this;
		}
	}
}
