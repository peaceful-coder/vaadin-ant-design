package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignMenuTheme {
	DARK("dark"),
	LIGHT("light"); //default

	@Getter
	private final String theme;
}
