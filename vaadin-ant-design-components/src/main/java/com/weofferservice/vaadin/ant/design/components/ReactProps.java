package com.weofferservice.vaadin.ant.design.components;

import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

import java.util.Map;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@Value
@NonFinal
@SuperBuilder
public abstract class ReactProps implements JsObject {

	UUID key;

	String className;

	Map<String, Object> style;
}
