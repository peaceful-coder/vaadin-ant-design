package com.weofferservice.vaadin.ant.design.components.layout.grid.row;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignRowProps extends ReactProps {

	String justify;

	String align;

	Boolean wrap;

	List<ReactProps> gutter;
}
