package com.weofferservice.vaadin.ant.design.components.layout.divider;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignDividerProps extends ReactProps {

	String children;

	Boolean dashed;

	Boolean plain;

	String orientation;

	String type;
}
