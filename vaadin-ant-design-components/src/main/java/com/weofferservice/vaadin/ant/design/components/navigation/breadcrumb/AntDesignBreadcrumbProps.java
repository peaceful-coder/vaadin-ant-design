package com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignBreadcrumbProps extends ReactProps {

	String separator;
}
