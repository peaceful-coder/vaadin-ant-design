package com.weofferservice.vaadin.ant.design.components.general.button;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@NonFinal
@SuperBuilder
public class AntDesignButtonProps extends ReactProps {

	ReactComponent children;

	String shape;

	String type;

	String size;

	Boolean disabled;

	Boolean loading;

	Boolean ghost;

	Boolean danger;

	Boolean block;

	ReactComponent icon;

	JsFunction onClick;
}
