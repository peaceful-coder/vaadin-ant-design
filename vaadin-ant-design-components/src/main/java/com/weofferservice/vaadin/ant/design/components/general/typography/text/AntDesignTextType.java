package com.weofferservice.vaadin.ant.design.components.general.typography.text;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignTextType {
	SECONDARY("secondary"),
	SUCCESS("success"),
	WARNING("warning"),
	DANGER("danger");

	@Getter
	private final String type;
}
