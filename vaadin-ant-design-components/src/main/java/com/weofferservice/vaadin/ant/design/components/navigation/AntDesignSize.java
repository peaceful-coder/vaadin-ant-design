package com.weofferservice.vaadin.ant.design.components.navigation;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignSize {
	DEFAULT("default"),
	SMALL("small");

	@Getter
	private final String size;
}
