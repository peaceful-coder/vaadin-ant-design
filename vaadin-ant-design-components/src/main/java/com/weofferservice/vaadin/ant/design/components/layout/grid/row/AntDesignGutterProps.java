package com.weofferservice.vaadin.ant.design.components.layout.grid.row;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignGutterProps extends ReactProps {

	Integer xs;

	Integer sm;

	Integer md;

	Integer lg;
}