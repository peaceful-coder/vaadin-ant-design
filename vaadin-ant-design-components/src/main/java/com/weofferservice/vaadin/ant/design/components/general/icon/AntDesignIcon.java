package com.weofferservice.vaadin.ant.design.components.general.icon;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignIcon extends AntDesignBaseComponent {

	private AntDesignIcon(@NonNull UUID antDesignBaseComponentUuid,
						  @NonNull AntDesignIconType antDesignIconType,
						  @NonNull AntDesignIconProps antDesignIconProps,
						  String svgComponentName) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name(antDesignIconType.getIconName())
						.props(antDesignIconProps)
						.children(
								Objects.nonNull(svgComponentName)
										? Collections.singletonList(
										ReactComponent.builder()
												.name(svgComponentName)
												.props(SvgProps.builder().build())
												.build())
										: null
						)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private AntDesignIconType antDesignIconType = AntDesignIconType.ICON;

		private String svgComponentName = null;

		private final AntDesignIconProps.AntDesignIconPropsBuilder propsBuilder = AntDesignIconProps.builder();

		public AntDesignIcon build() {
			if (antDesignIconType == AntDesignIconType.ICON
					&& Objects.isNull(svgComponentName)) {
				throw new IllegalStateException(
						antDesignIconType + " must contain 'svgComponentName', but 'svgComponentName' was " + svgComponentName);
			}
			propsBuilder.key(key);
			return new AntDesignIcon(
					antDesignBaseComponentUuid,
					antDesignIconType,
					propsBuilder.build(),
					svgComponentName);
		}

		public Builder iconType(@NonNull AntDesignIconType antDesignIconType) {
			this.antDesignIconType = antDesignIconType;
			return this;
		}

		public Builder svgComponentName(@NonNull String svgComponentName) {
			this.svgComponentName = svgComponentName;
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder rotate(int rotate) {
			propsBuilder.rotate(rotate);
			return this;
		}

		public Builder spin(boolean spin) {
			propsBuilder.spin(spin);
			return this;
		}

		public Builder twoToneColor(@NonNull String twoToneColor) {
			propsBuilder.twoToneColor(twoToneColor);
			return this;
		}
	}
}
