package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.editable;

import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignEditableProps extends ReactProps {

	ReactComponent icon;

	String tooltip;
}
