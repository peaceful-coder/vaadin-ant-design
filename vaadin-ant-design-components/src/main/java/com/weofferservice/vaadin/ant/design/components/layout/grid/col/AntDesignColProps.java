package com.weofferservice.vaadin.ant.design.components.layout.grid.col;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignColProps extends ReactProps {

	Integer span;

	Integer offset;

	Integer push;

	Integer pull;

	Integer order;

	String flex;

	ReactProps xs;

	ReactProps sm;

	ReactProps md;

	ReactProps lg;
}
