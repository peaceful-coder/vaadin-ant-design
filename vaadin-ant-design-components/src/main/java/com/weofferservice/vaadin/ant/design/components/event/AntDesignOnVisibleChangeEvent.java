package com.weofferservice.vaadin.ant.design.components.event;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import lombok.Getter;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@DomEvent(AntDesignOnVisibleChangeEvent.NAME)
public class AntDesignOnVisibleChangeEvent extends AntDesignEvent {

	public static final String NAME = "ant-on-visible-change-event";

	@Getter
	private final boolean visible;

	public AntDesignOnVisibleChangeEvent(@NonNull AntDesignBaseComponent source,
										 boolean fromClient,
										 @NonNull @EventData("event.antDesignBaseComponentUuid") String antDesignBaseComponentUuid,
										 @NonNull @EventData("event.key") String key,
										 @EventData("event.visible") boolean visible) {
		super(
				source,
				fromClient,
				UUID.fromString(antDesignBaseComponentUuid),
				UUID.fromString(key));
		this.visible = visible;
	}
}