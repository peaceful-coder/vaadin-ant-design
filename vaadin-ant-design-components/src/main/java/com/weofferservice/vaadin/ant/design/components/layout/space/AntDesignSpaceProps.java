package com.weofferservice.vaadin.ant.design.components.layout.space;

import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignSpaceProps extends ReactProps {

	String direction;

	String size;

	String align;

	ReactComponent split;
}
