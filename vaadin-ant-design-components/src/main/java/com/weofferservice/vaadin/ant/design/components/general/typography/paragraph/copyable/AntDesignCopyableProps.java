package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.copyable;

import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignCopyableProps extends ReactProps {

	List<ReactComponent> icon;

	List<String> tooltips;
}
