package com.weofferservice.vaadin.ant.design.components.general.icon;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignIconType {
	ICON("Icon"),
	ACCOUNT_BOOK_FILLED("AccountBookFilled"),
	ACCOUNT_BOOK_OUTLINED("AccountBookOutlined"),
	ACCOUNT_BOOK_TWO_TONE("AccountBookTwoTone"),
	AIM_OUTLINED("AimOutlined"),
	ALERT_FILLED("AlertFilled"),
	ALERT_OUTLINED("AlertOutlined"),
	ALERT_TWO_TONE("AlertTwoTone"),
	ALIBABA_OUTLINED("AlibabaOutlined"),
	ALIGN_CENTER_OUTLINED("AlignCenterOutlined"),
	ALIGN_LEFT_OUTLINED("AlignLeftOutlined"),
	ALIGN_RIGHT_OUTLINED("AlignRightOutlined"),
	ALIPAY_CIRCLE_FILLED("AlipayCircleFilled"),
	ALIPAY_CIRCLE_OUTLINED("AlipayCircleOutlined"),
	ALIPAY_OUTLINED("AlipayOutlined"),
	ALIPAY_SQUARE_FILLED("AlipaySquareFilled"),
	ALIWANGWANG_FILLED("AliwangwangFilled"),
	ALIWANGWANG_OUTLINED("AliwangwangOutlined"),
	ALIYUN_OUTLINED("AliyunOutlined"),
	AMAZON_CIRCLE_FILLED("AmazonCircleFilled"),
	AMAZON_OUTLINED("AmazonOutlined"),
	AMAZON_SQUARE_FILLED("AmazonSquareFilled"),
	ANDROID_FILLED("AndroidFilled"),
	ANDROID_OUTLINED("AndroidOutlined"),
	ANT_CLOUD_OUTLINED("AntCloudOutlined"),
	ANT_DESIGN_OUTLINED("AntDesignOutlined"),
	APARTMENT_OUTLINED("ApartmentOutlined"),
	API_FILLED("ApiFilled"),
	API_OUTLINED("ApiOutlined"),
	API_TWO_TONE("ApiTwoTone"),
	APPLE_FILLED("AppleFilled"),
	APPLE_OUTLINED("AppleOutlined"),
	APPSTORE_ADD_OUTLINED("AppstoreAddOutlined"),
	APPSTORE_FILLED("AppstoreFilled"),
	APPSTORE_OUTLINED("AppstoreOutlined"),
	APPSTORE_TWO_TONE("AppstoreTwoTone"),
	AREA_CHART_OUTLINED("AreaChartOutlined"),
	ARROW_DOWN_OUTLINED("ArrowDownOutlined"),
	ARROW_LEFT_OUTLINED("ArrowLeftOutlined"),
	ARROW_RIGHT_OUTLINED("ArrowRightOutlined"),
	ARROW_UP_OUTLINED("ArrowUpOutlined"),
	ARROWS_ALT_OUTLINED("ArrowsAltOutlined"),
	AUDIO_FILLED("AudioFilled"),
	AUDIO_MUTED_OUTLINED("AudioMutedOutlined"),
	AUDIO_OUTLINED("AudioOutlined"),
	AUDIO_TWO_TONE("AudioTwoTone"),
	AUDIT_OUTLINED("AuditOutlined"),
	BACKWARD_FILLED("BackwardFilled"),
	BACKWARD_OUTLINED("BackwardOutlined"),
	BANK_FILLED("BankFilled"),
	BANK_OUTLINED("BankOutlined"),
	BANK_TWO_TONE("BankTwoTone"),
	BAR_CHART_OUTLINED("BarChartOutlined"),
	BARCODE_OUTLINED("BarcodeOutlined"),
	BARS_OUTLINED("BarsOutlined"),
	BEHANCE_CIRCLE_FILLED("BehanceCircleFilled"),
	BEHANCE_OUTLINED("BehanceOutlined"),
	BEHANCE_SQUARE_FILLED("BehanceSquareFilled"),
	BEHANCE_SQUARE_OUTLINED("BehanceSquareOutlined"),
	BELL_FILLED("BellFilled"),
	BELL_OUTLINED("BellOutlined"),
	BELL_TWO_TONE("BellTwoTone"),
	BG_COLORS_OUTLINED("BgColorsOutlined"),
	BLOCK_OUTLINED("BlockOutlined"),
	BOLD_OUTLINED("BoldOutlined"),
	BOOK_FILLED("BookFilled"),
	BOOK_OUTLINED("BookOutlined"),
	BOOK_TWO_TONE("BookTwoTone"),
	BORDER_BOTTOM_OUTLINED("BorderBottomOutlined"),
	BORDER_HORIZONTAL_OUTLINED("BorderHorizontalOutlined"),
	BORDER_INNER_OUTLINED("BorderInnerOutlined"),
	BORDER_LEFT_OUTLINED("BorderLeftOutlined"),
	BORDER_OUTER_OUTLINED("BorderOuterOutlined"),
	BORDER_OUTLINED("BorderOutlined"),
	BORDER_RIGHT_OUTLINED("BorderRightOutlined"),
	BORDER_TOP_OUTLINED("BorderTopOutlined"),
	BORDER_VERTICLE_OUTLINED("BorderVerticleOutlined"),
	BORDERLESS_TABLE_OUTLINED("BorderlessTableOutlined"),
	BOX_PLOT_FILLED("BoxPlotFilled"),
	BOX_PLOT_OUTLINED("BoxPlotOutlined"),
	BOX_PLOT_TWO_TONE("BoxPlotTwoTone"),
	BRANCHES_OUTLINED("BranchesOutlined"),
	BUG_FILLED("BugFilled"),
	BUG_OUTLINED("BugOutlined"),
	BUG_TWO_TONE("BugTwoTone"),
	BUILD_FILLED("BuildFilled"),
	BUILD_OUTLINED("BuildOutlined"),
	BUILD_TWO_TONE("BuildTwoTone"),
	BULB_FILLED("BulbFilled"),
	BULB_OUTLINED("BulbOutlined"),
	BULB_TWO_TONE("BulbTwoTone"),
	CALCULATOR_FILLED("CalculatorFilled"),
	CALCULATOR_OUTLINED("CalculatorOutlined"),
	CALCULATOR_TWO_TONE("CalculatorTwoTone"),
	CALENDAR_FILLED("CalendarFilled"),
	CALENDAR_OUTLINED("CalendarOutlined"),
	CALENDAR_TWO_TONE("CalendarTwoTone"),
	CAMERA_FILLED("CameraFilled"),
	CAMERA_OUTLINED("CameraOutlined"),
	CAMERA_TWO_TONE("CameraTwoTone"),
	CAR_FILLED("CarFilled"),
	CAR_OUTLINED("CarOutlined"),
	CAR_TWO_TONE("CarTwoTone"),
	CARET_DOWN_FILLED("CaretDownFilled"),
	CARET_DOWN_OUTLINED("CaretDownOutlined"),
	CARET_LEFT_FILLED("CaretLeftFilled"),
	CARET_LEFT_OUTLINED("CaretLeftOutlined"),
	CARET_RIGHT_FILLED("CaretRightFilled"),
	CARET_RIGHT_OUTLINED("CaretRightOutlined"),
	CARET_UP_FILLED("CaretUpFilled"),
	CARET_UP_OUTLINED("CaretUpOutlined"),
	CARRY_OUT_FILLED("CarryOutFilled"),
	CARRY_OUT_OUTLINED("CarryOutOutlined"),
	CARRY_OUT_TWO_TONE("CarryOutTwoTone"),
	CHECK_CIRCLE_FILLED("CheckCircleFilled"),
	CHECK_CIRCLE_OUTLINED("CheckCircleOutlined"),
	CHECK_CIRCLE_TWO_TONE("CheckCircleTwoTone"),
	CHECK_OUTLINED("CheckOutlined"),
	CHECK_SQUARE_FILLED("CheckSquareFilled"),
	CHECK_SQUARE_OUTLINED("CheckSquareOutlined"),
	CHECK_SQUARE_TWO_TONE("CheckSquareTwoTone"),
	CHROME_FILLED("ChromeFilled"),
	CHROME_OUTLINED("ChromeOutlined"),
	CI_CIRCLE_FILLED("CiCircleFilled"),
	CI_CIRCLE_OUTLINED("CiCircleOutlined"),
	CI_CIRCLE_TWO_TONE("CiCircleTwoTone"),
	CI_OUTLINED("CiOutlined"),
	CI_TWO_TONE("CiTwoTone"),
	CLEAR_OUTLINED("ClearOutlined"),
	CLOCK_CIRCLE_FILLED("ClockCircleFilled"),
	CLOCK_CIRCLE_OUTLINED("ClockCircleOutlined"),
	CLOCK_CIRCLE_TWO_TONE("ClockCircleTwoTone"),
	CLOSE_CIRCLE_FILLED("CloseCircleFilled"),
	CLOSE_CIRCLE_OUTLINED("CloseCircleOutlined"),
	CLOSE_CIRCLE_TWO_TONE("CloseCircleTwoTone"),
	CLOSE_OUTLINED("CloseOutlined"),
	CLOSE_SQUARE_FILLED("CloseSquareFilled"),
	CLOSE_SQUARE_OUTLINED("CloseSquareOutlined"),
	CLOSE_SQUARE_TWO_TONE("CloseSquareTwoTone"),
	CLOUD_DOWNLOAD_OUTLINED("CloudDownloadOutlined"),
	CLOUD_FILLED("CloudFilled"),
	CLOUD_OUTLINED("CloudOutlined"),
	CLOUD_SERVER_OUTLINED("CloudServerOutlined"),
	CLOUD_SYNC_OUTLINED("CloudSyncOutlined"),
	CLOUD_TWO_TONE("CloudTwoTone"),
	CLOUD_UPLOAD_OUTLINED("CloudUploadOutlined"),
	CLUSTER_OUTLINED("ClusterOutlined"),
	CODE_FILLED("CodeFilled"),
	CODE_OUTLINED("CodeOutlined"),
	CODE_SANDBOX_CIRCLE_FILLED("CodeSandboxCircleFilled"),
	CODE_SANDBOX_OUTLINED("CodeSandboxOutlined"),
	CODE_SANDBOX_SQUARE_FILLED("CodeSandboxSquareFilled"),
	CODE_TWO_TONE("CodeTwoTone"),
	CODEPEN_CIRCLE_FILLED("CodepenCircleFilled"),
	CODEPEN_CIRCLE_OUTLINED("CodepenCircleOutlined"),
	CODEPEN_OUTLINED("CodepenOutlined"),
	CODEPEN_SQUARE_FILLED("CodepenSquareFilled"),
	COFFEE_OUTLINED("CoffeeOutlined"),
	COLUMN_HEIGHT_OUTLINED("ColumnHeightOutlined"),
	COLUMN_WIDTH_OUTLINED("ColumnWidthOutlined"),
	COMMENT_OUTLINED("CommentOutlined"),
	COMPASS_FILLED("CompassFilled"),
	COMPASS_OUTLINED("CompassOutlined"),
	COMPASS_TWO_TONE("CompassTwoTone"),
	COMPRESS_OUTLINED("CompressOutlined"),
	CONSOLE_SQL_OUTLINED("ConsoleSqlOutlined"),
	CONTACTS_FILLED("ContactsFilled"),
	CONTACTS_OUTLINED("ContactsOutlined"),
	CONTACTS_TWO_TONE("ContactsTwoTone"),
	CONTAINER_FILLED("ContainerFilled"),
	CONTAINER_OUTLINED("ContainerOutlined"),
	CONTAINER_TWO_TONE("ContainerTwoTone"),
	CONTROL_FILLED("ControlFilled"),
	CONTROL_OUTLINED("ControlOutlined"),
	CONTROL_TWO_TONE("ControlTwoTone"),
	COPY_FILLED("CopyFilled"),
	COPY_OUTLINED("CopyOutlined"),
	COPY_TWO_TONE("CopyTwoTone"),
	COPYRIGHT_CIRCLE_FILLED("CopyrightCircleFilled"),
	COPYRIGHT_CIRCLE_OUTLINED("CopyrightCircleOutlined"),
	COPYRIGHT_CIRCLE_TWO_TONE("CopyrightCircleTwoTone"),
	COPYRIGHT_OUTLINED("CopyrightOutlined"),
	COPYRIGHT_TWO_TONE("CopyrightTwoTone"),
	CREDIT_CARD_FILLED("CreditCardFilled"),
	CREDIT_CARD_OUTLINED("CreditCardOutlined"),
	CREDIT_CARD_TWO_TONE("CreditCardTwoTone"),
	CROWN_FILLED("CrownFilled"),
	CROWN_OUTLINED("CrownOutlined"),
	CROWN_TWO_TONE("CrownTwoTone"),
	CUSTOMER_SERVICE_FILLED("CustomerServiceFilled"),
	CUSTOMER_SERVICE_OUTLINED("CustomerServiceOutlined"),
	CUSTOMER_SERVICE_TWO_TONE("CustomerServiceTwoTone"),
	DASH_OUTLINED("DashOutlined"),
	DASHBOARD_FILLED("DashboardFilled"),
	DASHBOARD_OUTLINED("DashboardOutlined"),
	DASHBOARD_TWO_TONE("DashboardTwoTone"),
	DATABASE_FILLED("DatabaseFilled"),
	DATABASE_OUTLINED("DatabaseOutlined"),
	DATABASE_TWO_TONE("DatabaseTwoTone"),
	DELETE_COLUMN_OUTLINED("DeleteColumnOutlined"),
	DELETE_FILLED("DeleteFilled"),
	DELETE_OUTLINED("DeleteOutlined"),
	DELETE_ROW_OUTLINED("DeleteRowOutlined"),
	DELETE_TWO_TONE("DeleteTwoTone"),
	DELIVERED_PROCEDURE_OUTLINED("DeliveredProcedureOutlined"),
	DEPLOYMENT_UNIT_OUTLINED("DeploymentUnitOutlined"),
	DESKTOP_OUTLINED("DesktopOutlined"),
	DIFF_FILLED("DiffFilled"),
	DIFF_OUTLINED("DiffOutlined"),
	DIFF_TWO_TONE("DiffTwoTone"),
	DINGDING_OUTLINED("DingdingOutlined"),
	DINGTALK_CIRCLE_FILLED("DingtalkCircleFilled"),
	DINGTALK_OUTLINED("DingtalkOutlined"),
	DINGTALK_SQUARE_FILLED("DingtalkSquareFilled"),
	DISCONNECT_OUTLINED("DisconnectOutlined"),
	DISLIKE_FILLED("DislikeFilled"),
	DISLIKE_OUTLINED("DislikeOutlined"),
	DISLIKE_TWO_TONE("DislikeTwoTone"),
	DOLLAR_CIRCLE_FILLED("DollarCircleFilled"),
	DOLLAR_CIRCLE_OUTLINED("DollarCircleOutlined"),
	DOLLAR_CIRCLE_TWO_TONE("DollarCircleTwoTone"),
	DOLLAR_OUTLINED("DollarOutlined"),
	DOLLAR_TWO_TONE("DollarTwoTone"),
	DOT_CHART_OUTLINED("DotChartOutlined"),
	DOUBLE_LEFT_OUTLINED("DoubleLeftOutlined"),
	DOUBLE_RIGHT_OUTLINED("DoubleRightOutlined"),
	DOWN_CIRCLE_FILLED("DownCircleFilled"),
	DOWN_CIRCLE_OUTLINED("DownCircleOutlined"),
	DOWN_CIRCLE_TWO_TONE("DownCircleTwoTone"),
	DOWN_OUTLINED("DownOutlined"),
	DOWN_SQUARE_FILLED("DownSquareFilled"),
	DOWN_SQUARE_OUTLINED("DownSquareOutlined"),
	DOWN_SQUARE_TWO_TONE("DownSquareTwoTone"),
	DOWNLOAD_OUTLINED("DownloadOutlined"),
	DRAG_OUTLINED("DragOutlined"),
	DRIBBBLE_CIRCLE_FILLED("DribbbleCircleFilled"),
	DRIBBBLE_OUTLINED("DribbbleOutlined"),
	DRIBBBLE_SQUARE_FILLED("DribbbleSquareFilled"),
	DRIBBBLE_SQUARE_OUTLINED("DribbbleSquareOutlined"),
	DROPBOX_CIRCLE_FILLED("DropboxCircleFilled"),
	DROPBOX_OUTLINED("DropboxOutlined"),
	DROPBOX_SQUARE_FILLED("DropboxSquareFilled"),
	EDIT_FILLED("EditFilled"),
	EDIT_OUTLINED("EditOutlined"),
	EDIT_TWO_TONE("EditTwoTone"),
	ELLIPSIS_OUTLINED("EllipsisOutlined"),
	ENTER_OUTLINED("EnterOutlined"),
	ENVIRONMENT_FILLED("EnvironmentFilled"),
	ENVIRONMENT_OUTLINED("EnvironmentOutlined"),
	ENVIRONMENT_TWO_TONE("EnvironmentTwoTone"),
	EURO_CIRCLE_FILLED("EuroCircleFilled"),
	EURO_CIRCLE_OUTLINED("EuroCircleOutlined"),
	EURO_CIRCLE_TWO_TONE("EuroCircleTwoTone"),
	EURO_OUTLINED("EuroOutlined"),
	EURO_TWO_TONE("EuroTwoTone"),
	EXCEPTION_OUTLINED("ExceptionOutlined"),
	EXCLAMATION_CIRCLE_FILLED("ExclamationCircleFilled"),
	EXCLAMATION_CIRCLE_OUTLINED("ExclamationCircleOutlined"),
	EXCLAMATION_CIRCLE_TWO_TONE("ExclamationCircleTwoTone"),
	EXCLAMATION_OUTLINED("ExclamationOutlined"),
	EXPAND_ALT_OUTLINED("ExpandAltOutlined"),
	EXPAND_OUTLINED("ExpandOutlined"),
	EXPERIMENT_FILLED("ExperimentFilled"),
	EXPERIMENT_OUTLINED("ExperimentOutlined"),
	EXPERIMENT_TWO_TONE("ExperimentTwoTone"),
	EXPORT_OUTLINED("ExportOutlined"),
	EYE_FILLED("EyeFilled"),
	EYE_INVISIBLE_FILLED("EyeInvisibleFilled"),
	EYE_INVISIBLE_OUTLINED("EyeInvisibleOutlined"),
	EYE_INVISIBLE_TWO_TONE("EyeInvisibleTwoTone"),
	EYE_OUTLINED("EyeOutlined"),
	EYE_TWO_TONE("EyeTwoTone"),
	FACEBOOK_FILLED("FacebookFilled"),
	FACEBOOK_OUTLINED("FacebookOutlined"),
	FALL_OUTLINED("FallOutlined"),
	FAST_BACKWARD_FILLED("FastBackwardFilled"),
	FAST_BACKWARD_OUTLINED("FastBackwardOutlined"),
	FAST_FORWARD_FILLED("FastForwardFilled"),
	FAST_FORWARD_OUTLINED("FastForwardOutlined"),
	FIELD_BINARY_OUTLINED("FieldBinaryOutlined"),
	FIELD_NUMBER_OUTLINED("FieldNumberOutlined"),
	FIELD_STRING_OUTLINED("FieldStringOutlined"),
	FIELD_TIME_OUTLINED("FieldTimeOutlined"),
	FILE_ADD_FILLED("FileAddFilled"),
	FILE_ADD_OUTLINED("FileAddOutlined"),
	FILE_ADD_TWO_TONE("FileAddTwoTone"),
	FILE_DONE_OUTLINED("FileDoneOutlined"),
	FILE_EXCEL_FILLED("FileExcelFilled"),
	FILE_EXCEL_OUTLINED("FileExcelOutlined"),
	FILE_EXCEL_TWO_TONE("FileExcelTwoTone"),
	FILE_EXCLAMATION_FILLED("FileExclamationFilled"),
	FILE_EXCLAMATION_OUTLINED("FileExclamationOutlined"),
	FILE_EXCLAMATION_TWO_TONE("FileExclamationTwoTone"),
	FILE_FILLED("FileFilled"),
	FILE_GIF_OUTLINED("FileGifOutlined"),
	FILE_IMAGE_FILLED("FileImageFilled"),
	FILE_IMAGE_OUTLINED("FileImageOutlined"),
	FILE_IMAGE_TWO_TONE("FileImageTwoTone"),
	FILE_JPG_OUTLINED("FileJpgOutlined"),
	FILE_MARKDOWN_FILLED("FileMarkdownFilled"),
	FILE_MARKDOWN_OUTLINED("FileMarkdownOutlined"),
	FILE_MARKDOWN_TWO_TONE("FileMarkdownTwoTone"),
	FILE_OUTLINED("FileOutlined"),
	FILE_PDF_FILLED("FilePdfFilled"),
	FILE_PDF_OUTLINED("FilePdfOutlined"),
	FILE_PDF_TWO_TONE("FilePdfTwoTone"),
	FILE_PPT_FILLED("FilePptFilled"),
	FILE_PPT_OUTLINED("FilePptOutlined"),
	FILE_PPT_TWO_TONE("FilePptTwoTone"),
	FILE_PROTECT_OUTLINED("FileProtectOutlined"),
	FILE_SEARCH_OUTLINED("FileSearchOutlined"),
	FILE_SYNC_OUTLINED("FileSyncOutlined"),
	FILE_TEXT_FILLED("FileTextFilled"),
	FILE_TEXT_OUTLINED("FileTextOutlined"),
	FILE_TEXT_TWO_TONE("FileTextTwoTone"),
	FILE_TWO_TONE("FileTwoTone"),
	FILE_UNKNOWN_FILLED("FileUnknownFilled"),
	FILE_UNKNOWN_OUTLINED("FileUnknownOutlined"),
	FILE_UNKNOWN_TWO_TONE("FileUnknownTwoTone"),
	FILE_WORD_FILLED("FileWordFilled"),
	FILE_WORD_OUTLINED("FileWordOutlined"),
	FILE_WORD_TWO_TONE("FileWordTwoTone"),
	FILE_ZIP_FILLED("FileZipFilled"),
	FILE_ZIP_OUTLINED("FileZipOutlined"),
	FILE_ZIP_TWO_TONE("FileZipTwoTone"),
	FILTER_FILLED("FilterFilled"),
	FILTER_OUTLINED("FilterOutlined"),
	FILTER_TWO_TONE("FilterTwoTone"),
	FIRE_FILLED("FireFilled"),
	FIRE_OUTLINED("FireOutlined"),
	FIRE_TWO_TONE("FireTwoTone"),
	FLAG_FILLED("FlagFilled"),
	FLAG_OUTLINED("FlagOutlined"),
	FLAG_TWO_TONE("FlagTwoTone"),
	FOLDER_ADD_FILLED("FolderAddFilled"),
	FOLDER_ADD_OUTLINED("FolderAddOutlined"),
	FOLDER_ADD_TWO_TONE("FolderAddTwoTone"),
	FOLDER_FILLED("FolderFilled"),
	FOLDER_OPEN_FILLED("FolderOpenFilled"),
	FOLDER_OPEN_OUTLINED("FolderOpenOutlined"),
	FOLDER_OPEN_TWO_TONE("FolderOpenTwoTone"),
	FOLDER_OUTLINED("FolderOutlined"),
	FOLDER_TWO_TONE("FolderTwoTone"),
	FOLDER_VIEW_OUTLINED("FolderViewOutlined"),
	FONT_COLORS_OUTLINED("FontColorsOutlined"),
	FONT_SIZE_OUTLINED("FontSizeOutlined"),
	FORK_OUTLINED("ForkOutlined"),
	FORM_OUTLINED("FormOutlined"),
	FORMAT_PAINTER_FILLED("FormatPainterFilled"),
	FORMAT_PAINTER_OUTLINED("FormatPainterOutlined"),
	FORWARD_FILLED("ForwardFilled"),
	FORWARD_OUTLINED("ForwardOutlined"),
	FROWN_FILLED("FrownFilled"),
	FROWN_OUTLINED("FrownOutlined"),
	FROWN_TWO_TONE("FrownTwoTone"),
	FULLSCREEN_EXIT_OUTLINED("FullscreenExitOutlined"),
	FULLSCREEN_OUTLINED("FullscreenOutlined"),
	FUNCTION_OUTLINED("FunctionOutlined"),
	FUND_FILLED("FundFilled"),
	FUND_OUTLINED("FundOutlined"),
	FUND_PROJECTION_SCREEN_OUTLINED("FundProjectionScreenOutlined"),
	FUND_TWO_TONE("FundTwoTone"),
	FUND_VIEW_OUTLINED("FundViewOutlined"),
	FUNNEL_PLOT_FILLED("FunnelPlotFilled"),
	FUNNEL_PLOT_OUTLINED("FunnelPlotOutlined"),
	FUNNEL_PLOT_TWO_TONE("FunnelPlotTwoTone"),
	GATEWAY_OUTLINED("GatewayOutlined"),
	GIF_OUTLINED("GifOutlined"),
	GIFT_FILLED("GiftFilled"),
	GIFT_OUTLINED("GiftOutlined"),
	GIFT_TWO_TONE("GiftTwoTone"),
	GITHUB_FILLED("GithubFilled"),
	GITHUB_OUTLINED("GithubOutlined"),
	GITLAB_FILLED("GitlabFilled"),
	GITLAB_OUTLINED("GitlabOutlined"),
	GLOBAL_OUTLINED("GlobalOutlined"),
	GOLD_FILLED("GoldFilled"),
	GOLD_OUTLINED("GoldOutlined"),
	GOLD_TWO_TONE("GoldTwoTone"),
	GOLDEN_FILLED("GoldenFilled"),
	GOOGLE_CIRCLE_FILLED("GoogleCircleFilled"),
	GOOGLE_OUTLINED("GoogleOutlined"),
	GOOGLE_PLUS_CIRCLE_FILLED("GooglePlusCircleFilled"),
	GOOGLE_PLUS_OUTLINED("GooglePlusOutlined"),
	GOOGLE_PLUS_SQUARE_FILLED("GooglePlusSquareFilled"),
	GOOGLE_SQUARE_FILLED("GoogleSquareFilled"),
	GROUP_OUTLINED("GroupOutlined"),
	HDD_FILLED("HddFilled"),
	HDD_OUTLINED("HddOutlined"),
	HDD_TWO_TONE("HddTwoTone"),
	HEART_FILLED("HeartFilled"),
	HEART_OUTLINED("HeartOutlined"),
	HEART_TWO_TONE("HeartTwoTone"),
	HEAT_MAP_OUTLINED("HeatMapOutlined"),
	HIGHLIGHT_FILLED("HighlightFilled"),
	HIGHLIGHT_OUTLINED("HighlightOutlined"),
	HIGHLIGHT_TWO_TONE("HighlightTwoTone"),
	HISTORY_OUTLINED("HistoryOutlined"),
	HOME_FILLED("HomeFilled"),
	HOME_OUTLINED("HomeOutlined"),
	HOME_TWO_TONE("HomeTwoTone"),
	HOURGLASS_FILLED("HourglassFilled"),
	HOURGLASS_OUTLINED("HourglassOutlined"),
	HOURGLASS_TWO_TONE("HourglassTwoTone"),
	HTML5_FILLED("Html5Filled"),
	HTML5_OUTLINED("Html5Outlined"),
	HTML5_TWO_TONE("Html5TwoTone"),
	IDCARD_FILLED("IdcardFilled"),
	IDCARD_OUTLINED("IdcardOutlined"),
	IDCARD_TWO_TONE("IdcardTwoTone"),
	IE_CIRCLE_FILLED("IeCircleFilled"),
	IE_OUTLINED("IeOutlined"),
	IE_SQUARE_FILLED("IeSquareFilled"),
	IMPORT_OUTLINED("ImportOutlined"),
	INBOX_OUTLINED("InboxOutlined"),
	INFO_CIRCLE_FILLED("InfoCircleFilled"),
	INFO_CIRCLE_OUTLINED("InfoCircleOutlined"),
	INFO_CIRCLE_TWO_TONE("InfoCircleTwoTone"),
	INFO_OUTLINED("InfoOutlined"),
	INSERT_ROW_ABOVE_OUTLINED("InsertRowAboveOutlined"),
	INSERT_ROW_BELOW_OUTLINED("InsertRowBelowOutlined"),
	INSERT_ROW_LEFT_OUTLINED("InsertRowLeftOutlined"),
	INSERT_ROW_RIGHT_OUTLINED("InsertRowRightOutlined"),
	INSTAGRAM_FILLED("InstagramFilled"),
	INSTAGRAM_OUTLINED("InstagramOutlined"),
	INSURANCE_FILLED("InsuranceFilled"),
	INSURANCE_OUTLINED("InsuranceOutlined"),
	INSURANCE_TWO_TONE("InsuranceTwoTone"),
	INTERACTION_FILLED("InteractionFilled"),
	INTERACTION_OUTLINED("InteractionOutlined"),
	INTERACTION_TWO_TONE("InteractionTwoTone"),
	ISSUES_CLOSE_OUTLINED("IssuesCloseOutlined"),
	ITALIC_OUTLINED("ItalicOutlined"),
	KEY_OUTLINED("KeyOutlined"),
	LAPTOP_OUTLINED("LaptopOutlined"),
	LAYOUT_FILLED("LayoutFilled"),
	LAYOUT_OUTLINED("LayoutOutlined"),
	LAYOUT_TWO_TONE("LayoutTwoTone"),
	LEFT_CIRCLE_FILLED("LeftCircleFilled"),
	LEFT_CIRCLE_OUTLINED("LeftCircleOutlined"),
	LEFT_CIRCLE_TWO_TONE("LeftCircleTwoTone"),
	LEFT_OUTLINED("LeftOutlined"),
	LEFT_SQUARE_FILLED("LeftSquareFilled"),
	LEFT_SQUARE_OUTLINED("LeftSquareOutlined"),
	LEFT_SQUARE_TWO_TONE("LeftSquareTwoTone"),
	LIKE_FILLED("LikeFilled"),
	LIKE_OUTLINED("LikeOutlined"),
	LIKE_TWO_TONE("LikeTwoTone"),
	LINE_CHART_OUTLINED("LineChartOutlined"),
	LINE_HEIGHT_OUTLINED("LineHeightOutlined"),
	LINE_OUTLINED("LineOutlined"),
	LINK_OUTLINED("LinkOutlined"),
	LINKEDIN_FILLED("LinkedinFilled"),
	LINKEDIN_OUTLINED("LinkedinOutlined"),
	LOADING3QUARTERS_OUTLINED("Loading3QuartersOutlined"),
	LOADING_OUTLINED("LoadingOutlined"),
	LOCK_FILLED("LockFilled"),
	LOCK_OUTLINED("LockOutlined"),
	LOCK_TWO_TONE("LockTwoTone"),
	LOGIN_OUTLINED("LoginOutlined"),
	LOGOUT_OUTLINED("LogoutOutlined"),
	MAC_COMMAND_FILLED("MacCommandFilled"),
	MAC_COMMAND_OUTLINED("MacCommandOutlined"),
	MAIL_FILLED("MailFilled"),
	MAIL_OUTLINED("MailOutlined"),
	MAIL_TWO_TONE("MailTwoTone"),
	MAN_OUTLINED("ManOutlined"),
	MEDICINE_BOX_FILLED("MedicineBoxFilled"),
	MEDICINE_BOX_OUTLINED("MedicineBoxOutlined"),
	MEDICINE_BOX_TWO_TONE("MedicineBoxTwoTone"),
	MEDIUM_CIRCLE_FILLED("MediumCircleFilled"),
	MEDIUM_OUTLINED("MediumOutlined"),
	MEDIUM_SQUARE_FILLED("MediumSquareFilled"),
	MEDIUM_WORKMARK_OUTLINED("MediumWorkmarkOutlined"),
	MEH_FILLED("MehFilled"),
	MEH_OUTLINED("MehOutlined"),
	MEH_TWO_TONE("MehTwoTone"),
	MENU_FOLD_OUTLINED("MenuFoldOutlined"),
	MENU_OUTLINED("MenuOutlined"),
	MENU_UNFOLD_OUTLINED("MenuUnfoldOutlined"),
	MERGE_CELLS_OUTLINED("MergeCellsOutlined"),
	MESSAGE_FILLED("MessageFilled"),
	MESSAGE_OUTLINED("MessageOutlined"),
	MESSAGE_TWO_TONE("MessageTwoTone"),
	MINUS_CIRCLE_FILLED("MinusCircleFilled"),
	MINUS_CIRCLE_OUTLINED("MinusCircleOutlined"),
	MINUS_CIRCLE_TWO_TONE("MinusCircleTwoTone"),
	MINUS_OUTLINED("MinusOutlined"),
	MINUS_SQUARE_FILLED("MinusSquareFilled"),
	MINUS_SQUARE_OUTLINED("MinusSquareOutlined"),
	MINUS_SQUARE_TWO_TONE("MinusSquareTwoTone"),
	MOBILE_FILLED("MobileFilled"),
	MOBILE_OUTLINED("MobileOutlined"),
	MOBILE_TWO_TONE("MobileTwoTone"),
	MONEY_COLLECT_FILLED("MoneyCollectFilled"),
	MONEY_COLLECT_OUTLINED("MoneyCollectOutlined"),
	MONEY_COLLECT_TWO_TONE("MoneyCollectTwoTone"),
	MONITOR_OUTLINED("MonitorOutlined"),
	MORE_OUTLINED("MoreOutlined"),
	NODE_COLLAPSE_OUTLINED("NodeCollapseOutlined"),
	NODE_EXPAND_OUTLINED("NodeExpandOutlined"),
	NODE_INDEX_OUTLINED("NodeIndexOutlined"),
	NOTIFICATION_FILLED("NotificationFilled"),
	NOTIFICATION_OUTLINED("NotificationOutlined"),
	NOTIFICATION_TWO_TONE("NotificationTwoTone"),
	NUMBER_OUTLINED("NumberOutlined"),
	ONE_TO_ONE_OUTLINED("OneToOneOutlined"),
	ORDERED_LIST_OUTLINED("OrderedListOutlined"),
	PAPER_CLIP_OUTLINED("PaperClipOutlined"),
	PARTITION_OUTLINED("PartitionOutlined"),
	PAUSE_CIRCLE_FILLED("PauseCircleFilled"),
	PAUSE_CIRCLE_OUTLINED("PauseCircleOutlined"),
	PAUSE_CIRCLE_TWO_TONE("PauseCircleTwoTone"),
	PAUSE_OUTLINED("PauseOutlined"),
	PAY_CIRCLE_FILLED("PayCircleFilled"),
	PAY_CIRCLE_OUTLINED("PayCircleOutlined"),
	PERCENTAGE_OUTLINED("PercentageOutlined"),
	PHONE_FILLED("PhoneFilled"),
	PHONE_OUTLINED("PhoneOutlined"),
	PHONE_TWO_TONE("PhoneTwoTone"),
	PIC_CENTER_OUTLINED("PicCenterOutlined"),
	PIC_LEFT_OUTLINED("PicLeftOutlined"),
	PIC_RIGHT_OUTLINED("PicRightOutlined"),
	PICTURE_FILLED("PictureFilled"),
	PICTURE_OUTLINED("PictureOutlined"),
	PICTURE_TWO_TONE("PictureTwoTone"),
	PIE_CHART_FILLED("PieChartFilled"),
	PIE_CHART_OUTLINED("PieChartOutlined"),
	PIE_CHART_TWO_TONE("PieChartTwoTone"),
	PLAY_CIRCLE_FILLED("PlayCircleFilled"),
	PLAY_CIRCLE_OUTLINED("PlayCircleOutlined"),
	PLAY_CIRCLE_TWO_TONE("PlayCircleTwoTone"),
	PLAY_SQUARE_FILLED("PlaySquareFilled"),
	PLAY_SQUARE_OUTLINED("PlaySquareOutlined"),
	PLAY_SQUARE_TWO_TONE("PlaySquareTwoTone"),
	PLUS_CIRCLE_FILLED("PlusCircleFilled"),
	PLUS_CIRCLE_OUTLINED("PlusCircleOutlined"),
	PLUS_CIRCLE_TWO_TONE("PlusCircleTwoTone"),
	PLUS_OUTLINED("PlusOutlined"),
	PLUS_SQUARE_FILLED("PlusSquareFilled"),
	PLUS_SQUARE_OUTLINED("PlusSquareOutlined"),
	PLUS_SQUARE_TWO_TONE("PlusSquareTwoTone"),
	POUND_CIRCLE_FILLED("PoundCircleFilled"),
	POUND_CIRCLE_OUTLINED("PoundCircleOutlined"),
	POUND_CIRCLE_TWO_TONE("PoundCircleTwoTone"),
	POUND_OUTLINED("PoundOutlined"),
	POWEROFF_OUTLINED("PoweroffOutlined"),
	PRINTER_FILLED("PrinterFilled"),
	PRINTER_OUTLINED("PrinterOutlined"),
	PRINTER_TWO_TONE("PrinterTwoTone"),
	PROFILE_FILLED("ProfileFilled"),
	PROFILE_OUTLINED("ProfileOutlined"),
	PROFILE_TWO_TONE("ProfileTwoTone"),
	PROJECT_FILLED("ProjectFilled"),
	PROJECT_OUTLINED("ProjectOutlined"),
	PROJECT_TWO_TONE("ProjectTwoTone"),
	PROPERTY_SAFETY_FILLED("PropertySafetyFilled"),
	PROPERTY_SAFETY_OUTLINED("PropertySafetyOutlined"),
	PROPERTY_SAFETY_TWO_TONE("PropertySafetyTwoTone"),
	PULL_REQUEST_OUTLINED("PullRequestOutlined"),
	PUSHPIN_FILLED("PushpinFilled"),
	PUSHPIN_OUTLINED("PushpinOutlined"),
	PUSHPIN_TWO_TONE("PushpinTwoTone"),
	QQ_CIRCLE_FILLED("QqCircleFilled"),
	QQ_OUTLINED("QqOutlined"),
	QQ_SQUARE_FILLED("QqSquareFilled"),
	QRCODE_OUTLINED("QrcodeOutlined"),
	QUESTION_CIRCLE_FILLED("QuestionCircleFilled"),
	QUESTION_CIRCLE_OUTLINED("QuestionCircleOutlined"),
	QUESTION_CIRCLE_TWO_TONE("QuestionCircleTwoTone"),
	QUESTION_OUTLINED("QuestionOutlined"),
	RADAR_CHART_OUTLINED("RadarChartOutlined"),
	RADIUS_BOTTOMLEFT_OUTLINED("RadiusBottomleftOutlined"),
	RADIUS_BOTTOMRIGHT_OUTLINED("RadiusBottomrightOutlined"),
	RADIUS_SETTING_OUTLINED("RadiusSettingOutlined"),
	RADIUS_UPLEFT_OUTLINED("RadiusUpleftOutlined"),
	RADIUS_UPRIGHT_OUTLINED("RadiusUprightOutlined"),
	READ_FILLED("ReadFilled"),
	READ_OUTLINED("ReadOutlined"),
	RECONCILIATION_FILLED("ReconciliationFilled"),
	RECONCILIATION_OUTLINED("ReconciliationOutlined"),
	RECONCILIATION_TWO_TONE("ReconciliationTwoTone"),
	RED_ENVELOPE_FILLED("RedEnvelopeFilled"),
	RED_ENVELOPE_OUTLINED("RedEnvelopeOutlined"),
	RED_ENVELOPE_TWO_TONE("RedEnvelopeTwoTone"),
	REDDIT_CIRCLE_FILLED("RedditCircleFilled"),
	REDDIT_OUTLINED("RedditOutlined"),
	REDDIT_SQUARE_FILLED("RedditSquareFilled"),
	REDO_OUTLINED("RedoOutlined"),
	RELOAD_OUTLINED("ReloadOutlined"),
	REST_FILLED("RestFilled"),
	REST_OUTLINED("RestOutlined"),
	REST_TWO_TONE("RestTwoTone"),
	RETWEET_OUTLINED("RetweetOutlined"),
	RIGHT_CIRCLE_FILLED("RightCircleFilled"),
	RIGHT_CIRCLE_OUTLINED("RightCircleOutlined"),
	RIGHT_CIRCLE_TWO_TONE("RightCircleTwoTone"),
	RIGHT_OUTLINED("RightOutlined"),
	RIGHT_SQUARE_FILLED("RightSquareFilled"),
	RIGHT_SQUARE_OUTLINED("RightSquareOutlined"),
	RIGHT_SQUARE_TWO_TONE("RightSquareTwoTone"),
	RISE_OUTLINED("RiseOutlined"),
	ROBOT_FILLED("RobotFilled"),
	ROBOT_OUTLINED("RobotOutlined"),
	ROCKET_FILLED("RocketFilled"),
	ROCKET_OUTLINED("RocketOutlined"),
	ROCKET_TWO_TONE("RocketTwoTone"),
	ROLLBACK_OUTLINED("RollbackOutlined"),
	ROTATE_LEFT_OUTLINED("RotateLeftOutlined"),
	ROTATE_RIGHT_OUTLINED("RotateRightOutlined"),
	SAFETY_CERTIFICATE_FILLED("SafetyCertificateFilled"),
	SAFETY_CERTIFICATE_OUTLINED("SafetyCertificateOutlined"),
	SAFETY_CERTIFICATE_TWO_TONE("SafetyCertificateTwoTone"),
	SAFETY_OUTLINED("SafetyOutlined"),
	SAVE_FILLED("SaveFilled"),
	SAVE_OUTLINED("SaveOutlined"),
	SAVE_TWO_TONE("SaveTwoTone"),
	SCAN_OUTLINED("ScanOutlined"),
	SCHEDULE_FILLED("ScheduleFilled"),
	SCHEDULE_OUTLINED("ScheduleOutlined"),
	SCHEDULE_TWO_TONE("ScheduleTwoTone"),
	SCISSOR_OUTLINED("ScissorOutlined"),
	SEARCH_OUTLINED("SearchOutlined"),
	SECURITY_SCAN_FILLED("SecurityScanFilled"),
	SECURITY_SCAN_OUTLINED("SecurityScanOutlined"),
	SECURITY_SCAN_TWO_TONE("SecurityScanTwoTone"),
	SELECT_OUTLINED("SelectOutlined"),
	SEND_OUTLINED("SendOutlined"),
	SETTING_FILLED("SettingFilled"),
	SETTING_OUTLINED("SettingOutlined"),
	SETTING_TWO_TONE("SettingTwoTone"),
	SHAKE_OUTLINED("ShakeOutlined"),
	SHARE_ALT_OUTLINED("ShareAltOutlined"),
	SHOP_FILLED("ShopFilled"),
	SHOP_OUTLINED("ShopOutlined"),
	SHOP_TWO_TONE("ShopTwoTone"),
	SHOPPING_CART_OUTLINED("ShoppingCartOutlined"),
	SHOPPING_FILLED("ShoppingFilled"),
	SHOPPING_OUTLINED("ShoppingOutlined"),
	SHOPPING_TWO_TONE("ShoppingTwoTone"),
	SHRINK_OUTLINED("ShrinkOutlined"),
	SIGNAL_FILLED("SignalFilled"),
	SISTERNODE_OUTLINED("SisternodeOutlined"),
	SKETCH_CIRCLE_FILLED("SketchCircleFilled"),
	SKETCH_OUTLINED("SketchOutlined"),
	SKETCH_SQUARE_FILLED("SketchSquareFilled"),
	SKIN_FILLED("SkinFilled"),
	SKIN_OUTLINED("SkinOutlined"),
	SKIN_TWO_TONE("SkinTwoTone"),
	SKYPE_FILLED("SkypeFilled"),
	SKYPE_OUTLINED("SkypeOutlined"),
	SLACK_CIRCLE_FILLED("SlackCircleFilled"),
	SLACK_OUTLINED("SlackOutlined"),
	SLACK_SQUARE_FILLED("SlackSquareFilled"),
	SLACK_SQUARE_OUTLINED("SlackSquareOutlined"),
	SLIDERS_FILLED("SlidersFilled"),
	SLIDERS_OUTLINED("SlidersOutlined"),
	SLIDERS_TWO_TONE("SlidersTwoTone"),
	SMALL_DASH_OUTLINED("SmallDashOutlined"),
	SMILE_FILLED("SmileFilled"),
	SMILE_OUTLINED("SmileOutlined"),
	SMILE_TWO_TONE("SmileTwoTone"),
	SNIPPETS_FILLED("SnippetsFilled"),
	SNIPPETS_OUTLINED("SnippetsOutlined"),
	SNIPPETS_TWO_TONE("SnippetsTwoTone"),
	SOLUTION_OUTLINED("SolutionOutlined"),
	SORT_ASCENDING_OUTLINED("SortAscendingOutlined"),
	SORT_DESCENDING_OUTLINED("SortDescendingOutlined"),
	SOUND_FILLED("SoundFilled"),
	SOUND_OUTLINED("SoundOutlined"),
	SOUND_TWO_TONE("SoundTwoTone"),
	SPLIT_CELLS_OUTLINED("SplitCellsOutlined"),
	STAR_FILLED("StarFilled"),
	STAR_OUTLINED("StarOutlined"),
	STAR_TWO_TONE("StarTwoTone"),
	STEP_BACKWARD_FILLED("StepBackwardFilled"),
	STEP_BACKWARD_OUTLINED("StepBackwardOutlined"),
	STEP_FORWARD_FILLED("StepForwardFilled"),
	STEP_FORWARD_OUTLINED("StepForwardOutlined"),
	STOCK_OUTLINED("StockOutlined"),
	STOP_FILLED("StopFilled"),
	STOP_OUTLINED("StopOutlined"),
	STOP_TWO_TONE("StopTwoTone"),
	STRIKETHROUGH_OUTLINED("StrikethroughOutlined"),
	SUBNODE_OUTLINED("SubnodeOutlined"),
	SWAP_LEFT_OUTLINED("SwapLeftOutlined"),
	SWAP_OUTLINED("SwapOutlined"),
	SWAP_RIGHT_OUTLINED("SwapRightOutlined"),
	SWITCHER_FILLED("SwitcherFilled"),
	SWITCHER_OUTLINED("SwitcherOutlined"),
	SWITCHER_TWO_TONE("SwitcherTwoTone"),
	SYNC_OUTLINED("SyncOutlined"),
	TABLE_OUTLINED("TableOutlined"),
	TABLET_FILLED("TabletFilled"),
	TABLET_OUTLINED("TabletOutlined"),
	TABLET_TWO_TONE("TabletTwoTone"),
	TAG_FILLED("TagFilled"),
	TAG_OUTLINED("TagOutlined"),
	TAG_TWO_TONE("TagTwoTone"),
	TAGS_FILLED("TagsFilled"),
	TAGS_OUTLINED("TagsOutlined"),
	TAGS_TWO_TONE("TagsTwoTone"),
	TAOBAO_CIRCLE_FILLED("TaobaoCircleFilled"),
	TAOBAO_CIRCLE_OUTLINED("TaobaoCircleOutlined"),
	TAOBAO_OUTLINED("TaobaoOutlined"),
	TAOBAO_SQUARE_FILLED("TaobaoSquareFilled"),
	TEAM_OUTLINED("TeamOutlined"),
	THUNDERBOLT_FILLED("ThunderboltFilled"),
	THUNDERBOLT_OUTLINED("ThunderboltOutlined"),
	THUNDERBOLT_TWO_TONE("ThunderboltTwoTone"),
	TO_TOP_OUTLINED("ToTopOutlined"),
	TOOL_FILLED("ToolFilled"),
	TOOL_OUTLINED("ToolOutlined"),
	TOOL_TWO_TONE("ToolTwoTone"),
	TRADEMARK_CIRCLE_FILLED("TrademarkCircleFilled"),
	TRADEMARK_CIRCLE_OUTLINED("TrademarkCircleOutlined"),
	TRADEMARK_CIRCLE_TWO_TONE("TrademarkCircleTwoTone"),
	TRADEMARK_OUTLINED("TrademarkOutlined"),
	TRANSACTION_OUTLINED("TransactionOutlined"),
	TRANSLATION_OUTLINED("TranslationOutlined"),
	TROPHY_FILLED("TrophyFilled"),
	TROPHY_OUTLINED("TrophyOutlined"),
	TROPHY_TWO_TONE("TrophyTwoTone"),
	TWITTER_CIRCLE_FILLED("TwitterCircleFilled"),
	TWITTER_OUTLINED("TwitterOutlined"),
	TWITTER_SQUARE_FILLED("TwitterSquareFilled"),
	UNDERLINE_OUTLINED("UnderlineOutlined"),
	UNDO_OUTLINED("UndoOutlined"),
	UNGROUP_OUTLINED("UngroupOutlined"),
	UNLOCK_FILLED("UnlockFilled"),
	UNLOCK_OUTLINED("UnlockOutlined"),
	UNLOCK_TWO_TONE("UnlockTwoTone"),
	UNORDERED_LIST_OUTLINED("UnorderedListOutlined"),
	UP_CIRCLE_FILLED("UpCircleFilled"),
	UP_CIRCLE_OUTLINED("UpCircleOutlined"),
	UP_CIRCLE_TWO_TONE("UpCircleTwoTone"),
	UP_OUTLINED("UpOutlined"),
	UP_SQUARE_FILLED("UpSquareFilled"),
	UP_SQUARE_OUTLINED("UpSquareOutlined"),
	UP_SQUARE_TWO_TONE("UpSquareTwoTone"),
	UPLOAD_OUTLINED("UploadOutlined"),
	USB_FILLED("UsbFilled"),
	USB_OUTLINED("UsbOutlined"),
	USB_TWO_TONE("UsbTwoTone"),
	USER_ADD_OUTLINED("UserAddOutlined"),
	USER_DELETE_OUTLINED("UserDeleteOutlined"),
	USER_OUTLINED("UserOutlined"),
	USER_SWITCH_OUTLINED("UserSwitchOutlined"),
	USERGROUP_ADD_OUTLINED("UsergroupAddOutlined"),
	USERGROUP_DELETE_OUTLINED("UsergroupDeleteOutlined"),
	VERIFIED_OUTLINED("VerifiedOutlined"),
	VERTICAL_ALIGN_BOTTOM_OUTLINED("VerticalAlignBottomOutlined"),
	VERTICAL_ALIGN_MIDDLE_OUTLINED("VerticalAlignMiddleOutlined"),
	VERTICAL_ALIGN_TOP_OUTLINED("VerticalAlignTopOutlined"),
	VERTICAL_LEFT_OUTLINED("VerticalLeftOutlined"),
	VERTICAL_RIGHT_OUTLINED("VerticalRightOutlined"),
	VIDEO_CAMERA_ADD_OUTLINED("VideoCameraAddOutlined"),
	VIDEO_CAMERA_FILLED("VideoCameraFilled"),
	VIDEO_CAMERA_OUTLINED("VideoCameraOutlined"),
	VIDEO_CAMERA_TWO_TONE("VideoCameraTwoTone"),
	WALLET_FILLED("WalletFilled"),
	WALLET_OUTLINED("WalletOutlined"),
	WALLET_TWO_TONE("WalletTwoTone"),
	WARNING_FILLED("WarningFilled"),
	WARNING_OUTLINED("WarningOutlined"),
	WARNING_TWO_TONE("WarningTwoTone"),
	WECHAT_FILLED("WechatFilled"),
	WECHAT_OUTLINED("WechatOutlined"),
	WEIBO_CIRCLE_FILLED("WeiboCircleFilled"),
	WEIBO_CIRCLE_OUTLINED("WeiboCircleOutlined"),
	WEIBO_OUTLINED("WeiboOutlined"),
	WEIBO_SQUARE_FILLED("WeiboSquareFilled"),
	WEIBO_SQUARE_OUTLINED("WeiboSquareOutlined"),
	WHATS_APP_OUTLINED("WhatsAppOutlined"),
	WIFI_OUTLINED("WifiOutlined"),
	WINDOWS_FILLED("WindowsFilled"),
	WINDOWS_OUTLINED("WindowsOutlined"),
	WOMAN_OUTLINED("WomanOutlined"),
	YAHOO_FILLED("YahooFilled"),
	YAHOO_OUTLINED("YahooOutlined"),
	YOUTUBE_FILLED("YoutubeFilled"),
	YOUTUBE_OUTLINED("YoutubeOutlined"),
	YUQUE_FILLED("YuqueFilled"),
	YUQUE_OUTLINED("YuqueOutlined"),
	ZHIHU_CIRCLE_FILLED("ZhihuCircleFilled"),
	ZHIHU_OUTLINED("ZhihuOutlined"),
	ZHIHU_SQUARE_FILLED("ZhihuSquareFilled"),
	ZOOM_IN_OUTLINED("ZoomInOutlined"),
	ZOOM_OUT_OUTLINED("ZoomOutOutlined");

	@Getter
	private final String iconName;
}
