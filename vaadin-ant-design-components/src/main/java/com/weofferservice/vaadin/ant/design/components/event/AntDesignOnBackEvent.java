package com.weofferservice.vaadin.ant.design.components.event;

import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.EventData;
import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@DomEvent(AntDesignOnBackEvent.NAME)
public class AntDesignOnBackEvent extends AntDesignEvent {

	public static final String NAME = "ant-on-back-event";

	public AntDesignOnBackEvent(@NonNull AntDesignBaseComponent source,
								boolean fromClient,
								@NonNull @EventData("event.antDesignBaseComponentUuid") String antDesignBaseComponentUuid,
								@NonNull @EventData("event.key") String key) {
		super(
				source,
				fromClient,
				UUID.fromString(antDesignBaseComponentUuid),
				UUID.fromString(key));
	}
}