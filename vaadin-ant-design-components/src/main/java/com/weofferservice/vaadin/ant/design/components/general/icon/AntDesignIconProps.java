package com.weofferservice.vaadin.ant.design.components.general.icon;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignIconProps extends ReactProps {

	Boolean spin;

	Integer rotate;

	String twoToneColor;
}
