package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import lombok.NonNull;

import java.util.Map;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignStep extends AntDesignBaseComponent {

	private AntDesignStep(@NonNull UUID antDesignBaseComponentUuid,
						  @NonNull AntDesignStepProps antDesignStepProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Step")
						.props(antDesignStepProps)
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignStepProps.AntDesignStepPropsBuilder propsBuilder = AntDesignStepProps.builder();

		public AntDesignStep build() {
			propsBuilder.key(key);
			return new AntDesignStep(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder status(@NonNull AntDesignStepStatus antDesignStepStatus) {
			propsBuilder.status(antDesignStepStatus.getStatus());
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon antDesignIcon) {
			propsBuilder.icon(antDesignIcon.getComponent());
			return this;
		}

		public Builder title(@NonNull String title) {
			propsBuilder.title(title);
			return this;
		}

		public Builder subTitle(@NonNull String subTitle) {
			propsBuilder.subTitle(subTitle);
			return this;
		}

		public Builder description(@NonNull String description) {
			propsBuilder.description(description);
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}
	}
}
