package com.weofferservice.vaadin.ant.design.components.general.button;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignButtonShape {
	DEFAULT(null),
	CIRCLE("circle"),
	ROUND("round");

	@Getter
	private final String shape;
}
