package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnTitleClickEvent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignSubMenu extends AntDesignBaseComponent {

	private AntDesignSubMenu(@NonNull UUID antDesignBaseComponentUuid,
							 @NonNull AntDesignSubMenuProps antDesignSubMenuProps,
							 @NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("SubMenu")
						.props(antDesignSubMenuProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignSubMenuProps.AntDesignSubMenuPropsBuilder propsBuilder = AntDesignSubMenuProps.builder();

		private String className = null;

		public AntDesignSubMenu build() {
			propsBuilder
					.className(className)
					.key(key);
			return new AntDesignSubMenu(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder turnOnTitleClick() {
			final String classNameForReference = AntDesignSubMenu.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnTitleClickEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onTitleClick(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder addChild(@NonNull AntDesignBaseComponent antDesignComponent) {
			children.add(antDesignComponent);
			return this;
		}

		public Builder title(@NonNull String title) {
			propsBuilder.title(title);
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon antDesignIcon) {
			propsBuilder.icon(antDesignIcon.getComponent());
			return this;
		}

		public Builder className(@NonNull String className) {
			if (Objects.isNull(this.className)) {
				this.className = className;
			} else {
				this.className += " " + className;
			}
			return this;
		}
	}
}
