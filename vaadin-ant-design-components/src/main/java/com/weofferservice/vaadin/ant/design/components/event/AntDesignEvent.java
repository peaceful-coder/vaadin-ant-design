package com.weofferservice.vaadin.ant.design.components.event;

import com.vaadin.flow.component.ComponentEvent;
import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import lombok.Getter;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public abstract class AntDesignEvent extends ComponentEvent<AntDesignBaseComponent> {

	@Getter
	private final UUID antDesignBaseComponentUuid;

	@Getter
	private final UUID key;

	protected AntDesignEvent(@NonNull AntDesignBaseComponent source,
							 boolean fromClient,
							 @NonNull UUID antDesignBaseComponentUuid,
							 @NonNull UUID key) {
		super(source, fromClient);
		this.antDesignBaseComponentUuid = antDesignBaseComponentUuid;
		this.key = key;
	}
}