package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.copyable;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.AntDesignParagraphProps;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignCopyableParagraph extends AntDesignBaseComponent {

	private AntDesignCopyableParagraph(@NonNull UUID antDesignBaseComponentUuid,
									   @NonNull AntDesignParagraphProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Paragraph")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private AntDesignIcon iconBefore = null;

		private AntDesignIcon iconAfter = null;

		private String tooltipBefore = null;

		private String tooltipAfter = null;

		private final AntDesignParagraphProps.AntDesignParagraphPropsBuilder propsBuilder = AntDesignParagraphProps.builder();

		public AntDesignCopyableParagraph build() {
			final List<ReactComponent> icons = new ArrayList<>();
			if (Objects.nonNull(iconBefore)) {
				icons.add(iconBefore.getComponent());
			}
			if (Objects.nonNull(iconAfter)) {
				if (Objects.isNull(iconBefore)) {
					icons.add(null);
				}
				icons.add(iconAfter.getComponent());
			}
			final List<String> tooltips = new ArrayList<>();
			if (Objects.nonNull(tooltipBefore)) {
				tooltips.add(tooltipBefore);
			}
			if (Objects.nonNull(tooltipAfter)) {
				if (Objects.isNull(tooltipBefore)) {
					tooltips.add(null);
				}
				tooltips.add(tooltipAfter);
			}
			propsBuilder
					.copyable(
							AntDesignCopyableProps.builder()
									.icon(icons)
									.tooltips(tooltips)
									.build())
					.key(key);
			return new AntDesignCopyableParagraph(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder iconBefore(@NonNull AntDesignIcon icon) {
			this.iconBefore = icon;
			return this;
		}

		public Builder iconAfter(@NonNull AntDesignIcon icon) {
			this.iconAfter = icon;
			return this;
		}

		public Builder tooltipBefore(@NonNull String tooltip) {
			this.tooltipBefore = tooltip;
			return this;
		}

		public Builder tooltipAfter(@NonNull String tooltip) {
			this.tooltipAfter = tooltip;
			return this;
		}
	}
}
