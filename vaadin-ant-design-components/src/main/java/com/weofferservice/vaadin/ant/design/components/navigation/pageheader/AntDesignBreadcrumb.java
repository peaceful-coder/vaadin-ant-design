package com.weofferservice.vaadin.ant.design.components.navigation.pageheader;

import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignBreadcrumb {

	List<AntDesignRoute> routes;
}
