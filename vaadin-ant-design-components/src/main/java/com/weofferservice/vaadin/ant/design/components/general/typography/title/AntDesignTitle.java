package com.weofferservice.vaadin.ant.design.components.general.typography.title;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignTitle extends AntDesignBaseComponent {

	private AntDesignTitle(@NonNull UUID antDesignBaseComponentUuid,
						   @NonNull AntDesignTitleProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Title")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignTitleProps.AntDesignTitlePropsBuilder propsBuilder = AntDesignTitleProps.builder();

		public AntDesignTitle build() {
			propsBuilder.key(key);
			return new AntDesignTitle(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder level(@NonNull Integer level) {
			propsBuilder.level(level);
			return this;
		}
	}
}
