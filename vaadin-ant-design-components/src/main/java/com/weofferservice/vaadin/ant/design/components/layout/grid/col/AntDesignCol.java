package com.weofferservice.vaadin.ant.design.components.layout.grid.col;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import lombok.NonNull;

import java.util.Collections;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignCol extends AntDesignBaseComponent {

	private AntDesignCol(@NonNull UUID antDesignBaseComponentUuid,
						 @NonNull AntDesignColProps antDesignColProps,
						 @NonNull Tag tag) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Col")
						.props(antDesignColProps)
						.children(Collections.singletonList(
								tag.getComponent()))
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private Tag tag = null;

		private final AntDesignColProps.AntDesignColPropsBuilder propsBuilder = AntDesignColProps.builder();

		public AntDesignCol build() {
			propsBuilder.key(key);
			return new AntDesignCol(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					tag);
		}

		public Builder tag(@NonNull Tag tag) {
			this.tag = tag;
			return this;
		}

		public Builder span(int span) {
			propsBuilder.span(span);
			return this;
		}

		public Builder offset(int offset) {
			propsBuilder.offset(offset);
			return this;
		}

		public Builder push(int push) {
			propsBuilder.push(push);
			return this;
		}

		public Builder pull(int pull) {
			propsBuilder.pull(pull);
			return this;
		}

		public Builder order(int order) {
			propsBuilder.order(order);
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder flex(@NonNull String flex) {
			propsBuilder.flex(flex);
			return this;
		}

		public Builder flex(int flex) {
			propsBuilder.flex(String.valueOf(flex));
			return this;
		}

		public Builder xsOrder(int order) {
			propsBuilder.xs(AntDesignOrderProps.builder().order(order).build());
			return this;
		}

		public Builder smOrder(int order) {
			propsBuilder.sm(AntDesignOrderProps.builder().order(order).build());
			return this;
		}

		public Builder mdOrder(int order) {
			propsBuilder.md(AntDesignOrderProps.builder().order(order).build());
			return this;
		}

		public Builder lgOrder(int order) {
			propsBuilder.lg(AntDesignOrderProps.builder().order(order).build());
			return this;
		}
	}
}
