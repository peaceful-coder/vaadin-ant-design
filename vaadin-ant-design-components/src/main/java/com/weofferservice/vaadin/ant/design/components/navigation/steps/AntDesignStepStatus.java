package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignStepStatus {
	WAIT("wait"),
	PROCESS("process"),
	FINISH("finish"),
	ERROR("error");

	@Getter
	private final String status;
}
