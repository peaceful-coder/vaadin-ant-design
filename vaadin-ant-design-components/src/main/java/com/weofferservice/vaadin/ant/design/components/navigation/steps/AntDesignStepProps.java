package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignStepProps extends ReactProps {

	String title;

	String subTitle;

	String description;

	String status;

	ReactComponent icon;
}
