package com.weofferservice.vaadin.ant.design.components;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignDirectionType {
	HORIZONTAL("horizontal"), //default
	VERTICAL("vertical");

	@Getter
	private final String direction;
}
