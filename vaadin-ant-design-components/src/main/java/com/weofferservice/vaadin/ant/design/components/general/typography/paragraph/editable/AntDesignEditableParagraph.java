package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.editable;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.general.typography.paragraph.AntDesignParagraphProps;
import lombok.NonNull;

import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignEditableParagraph extends AntDesignBaseComponent {

	private AntDesignEditableParagraph(@NonNull UUID antDesignBaseComponentUuid,
									   @NonNull AntDesignParagraphProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Paragraph")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private AntDesignIcon icon = null;

		private String tooltip = null;

		private final AntDesignParagraphProps.AntDesignParagraphPropsBuilder propsBuilder = AntDesignParagraphProps.builder();

		public AntDesignEditableParagraph build() {
			propsBuilder
					.editable(
							AntDesignEditableProps.builder()
									.icon(Objects.nonNull(icon) ? icon.getComponent() : null)
									.tooltip(tooltip)
									.build())
					.key(key);
			return new AntDesignEditableParagraph(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon icon) {
			this.icon = icon;
			return this;
		}

		public Builder tooltip(@NonNull String tooltip) {
			this.tooltip = tooltip;
			return this;
		}
	}
}
