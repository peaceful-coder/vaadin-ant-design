package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignStepsProps extends ReactProps {

	Integer current;

	Integer percent;

	String size;

	String direction;

	String type;

	Boolean progressDot;
}
