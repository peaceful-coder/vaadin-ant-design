package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignMenuItemGroup extends AntDesignBaseComponent {

	private AntDesignMenuItemGroup(@NonNull UUID antDesignBaseComponentUuid,
								   @NonNull AntDesignMenuItemGroupProps antDesignMenuItemGroupProps,
								   @NonNull List<AntDesignMenuItem> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("MenuItemGroup")
						.props(antDesignMenuItemGroupProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignMenuItem> children = new ArrayList<>();

		private final AntDesignMenuItemGroupProps.AntDesignMenuItemGroupPropsBuilder propsBuilder =
				AntDesignMenuItemGroupProps.builder();

		public AntDesignMenuItemGroup build() {
			propsBuilder.key(key);
			return new AntDesignMenuItemGroup(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addMenuItem(@NonNull AntDesignMenuItem menuItem) {
			children.add(menuItem);
			return this;
		}

		public Builder title(@NonNull String title) {
			propsBuilder.title(title);
			return this;
		}
	}
}
