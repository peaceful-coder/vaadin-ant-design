package com.weofferservice.vaadin.ant.design.components.general.typography.link;

import com.weofferservice.vaadin.ant.design.components.general.typography.AntDesignBaseProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignLinkProps extends AntDesignBaseProps {

	String target;

	String href;
}
