package com.weofferservice.vaadin.ant.design.components.general.typography.text;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignText extends AntDesignBaseComponent {

	private AntDesignText(@NonNull UUID antDesignBaseComponentUuid,
						  @NonNull AntDesignTextProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Text")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignTextProps.AntDesignTextPropsBuilder propsBuilder = AntDesignTextProps.builder();

		public AntDesignText build() {
			propsBuilder.key(key);
			return new AntDesignText(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder type(@NonNull AntDesignTextType antDesignTextType) {
			propsBuilder.type(antDesignTextType.getType());
			return this;
		}

		public Builder disabled(boolean disabled) {
			propsBuilder.disabled(disabled);
			return this;
		}

		public Builder mark(boolean mark) {
			propsBuilder.mark(mark);
			return this;
		}

		public Builder code(boolean code) {
			propsBuilder.code(code);
			return this;
		}

		public Builder keyboard(boolean keyboard) {
			propsBuilder.keyboard(keyboard);
			return this;
		}

		public Builder underline(boolean underline) {
			propsBuilder.underline(underline);
			return this;
		}

		public Builder delete(boolean delete) {
			propsBuilder.delete(delete);
			return this;
		}

		public Builder strong(boolean strong) {
			propsBuilder.strong(strong);
			return this;
		}
	}
}
