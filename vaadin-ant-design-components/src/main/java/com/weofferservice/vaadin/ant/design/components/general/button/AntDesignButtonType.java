package com.weofferservice.vaadin.ant.design.components.general.button;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignButtonType {
	DEFAULT("default"),
	PRIMARY("primary"),
	GHOST("ghost"),
	DASHED("dashed"),
	LINK("link"),
	TEXT("text");

	@Getter
	private final String type;
}
