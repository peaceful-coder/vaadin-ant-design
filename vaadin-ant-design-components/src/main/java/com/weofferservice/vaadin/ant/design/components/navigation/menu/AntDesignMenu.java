package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnClickEvent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Peaceful Coder
 */
public class AntDesignMenu extends AntDesignBaseComponent {

	private AntDesignMenu(@NonNull UUID antDesignBaseComponentUuid,
						  @NonNull AntDesignMenuProps antDesignMenuProps,
						  @NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Menu")
						.props(antDesignMenuProps)
						.children(
								children.stream()
										.map(AntDesignBaseComponent::getComponent)
										.collect(Collectors.toList())
						)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private String className = null;

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<UUID> selectedKeys = new ArrayList<>();

		private final List<UUID> defaultSelectedKeys = new ArrayList<>();

		private final List<UUID> openKeys = new ArrayList<>();

		private final List<UUID> defaultOpenKeys = new ArrayList<>();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignMenuProps.AntDesignMenuPropsBuilder propsBuilder = AntDesignMenuProps.builder();

		public AntDesignMenu build() {
			propsBuilder
					.className(className)
					.key(key);
			return new AntDesignMenu(
					antDesignBaseComponentUuid,
					propsBuilder
							.selectedKeys(selectedKeys)
							.defaultSelectedKeys(defaultSelectedKeys)
							.openKeys(!openKeys.isEmpty() ? openKeys : null)
							.defaultOpenKeys(defaultOpenKeys)
							.build(),
					children);
		}

		public Builder turnOnClick() {
			final String classNameForReference = AntDesignMenu.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.argumentNames(Collections.singletonList(
							"{ item, key, keyPath, domEvent }"
					))
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnClickEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = key;", // menu item's key
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onClick(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder addChild(@NonNull AntDesignBaseComponent child) {
			children.add(child);
			return this;
		}

		public Builder markAsSelected(@NonNull AntDesignBaseComponent child) {
			selectedKeys.add(child.getKey());
			return this;
		}

		public Builder markAsDefaultSelected(@NonNull AntDesignBaseComponent child) {
			defaultSelectedKeys.add(child.getKey());
			return this;
		}

		public Builder markAsOpen(@NonNull AntDesignBaseComponent child) {
			openKeys.add(child.getKey());
			return this;
		}

		public Builder markAsDefaultOpen(@NonNull AntDesignBaseComponent child) {
			defaultOpenKeys.add(child.getKey());
			return this;
		}

		public Builder mode(@NonNull AntDesignMenuMode antDesignMenuMode) {
			propsBuilder.mode(antDesignMenuMode.getMode());
			return this;
		}

		public Builder theme(@NonNull AntDesignMenuTheme antDesignMenuTheme) {
			propsBuilder.theme(antDesignMenuTheme.getTheme());
			return this;
		}

		public Builder inlineCollapsed(boolean inlineCollapsed) {
			propsBuilder.inlineCollapsed(inlineCollapsed);
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}

		public Builder className(@NonNull String className) {
			if (Objects.isNull(this.className)) {
				this.className = className;
			} else {
				this.className += " " + className;
			}
			return this;
		}
	}
}
