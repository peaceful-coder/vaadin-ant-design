package com.weofferservice.vaadin.ant.design.components;

import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class JsFunction implements JsObject {

	List<String> argumentNames;

	List<String> bodyLines;
}
