package com.weofferservice.vaadin.ant.design.components.tag;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.Map;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class TagProps extends ReactProps {

	ReactProps dangerouslySetInnerHTML;

	@JsonIgnore
	Map<String, Object> attributes;

	@JsonAnyGetter
	public Map<String, Object> attributes() {
		return attributes;
	}
}
