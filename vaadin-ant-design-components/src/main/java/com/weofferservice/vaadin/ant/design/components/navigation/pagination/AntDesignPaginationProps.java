package com.weofferservice.vaadin.ant.design.components.navigation.pagination;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignPaginationProps extends ReactProps {

	Integer current;

	Integer defaultCurrent;

	Integer defaultPageSize;

	Integer total;

	Boolean showSizeChanger;

	Boolean disabled;

	Boolean showQuickJumper;

	Boolean simple;

	String size;
}
