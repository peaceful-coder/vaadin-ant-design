package com.weofferservice.vaadin.ant.design.components.tag;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class DangerouslySetInnerHTMLProps extends ReactProps {

	String __html;
}
