package com.weofferservice.vaadin.ant.design.components.navigation.pageheader;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignPageHeaderProps extends ReactProps {

	String title;

	String subTitle;

	Boolean ghost;

	AntDesignBreadcrumb breadcrumb;

	List<ReactComponent> tags;

	AntDesignAvatar avatar;

	List<ReactComponent> extra;

	ReactComponent footer;

	JsFunction onBack;
}
