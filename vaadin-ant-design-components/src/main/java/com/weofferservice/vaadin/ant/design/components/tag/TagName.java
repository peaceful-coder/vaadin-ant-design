package com.weofferservice.vaadin.ant.design.components.tag;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum TagName {
	A("a"),
	SPAN("span"),
	DIV("div");

	@Getter
	private final String name;
}
