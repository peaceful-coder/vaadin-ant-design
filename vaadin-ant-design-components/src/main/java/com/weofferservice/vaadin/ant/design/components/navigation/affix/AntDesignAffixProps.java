package com.weofferservice.vaadin.ant.design.components.navigation.affix;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignAffixProps extends ReactProps {

	Integer offsetTop;

	Integer offsetBottom;

	JsFunction target;
}
