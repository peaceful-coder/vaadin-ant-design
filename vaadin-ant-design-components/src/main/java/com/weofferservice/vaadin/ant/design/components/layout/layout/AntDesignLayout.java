package com.weofferservice.vaadin.ant.design.components.layout.layout;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnCollapseEvent;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignLayout extends AntDesignBaseComponent {

	private final AntDesignLayoutType layoutType;

	private AntDesignLayout(@NonNull UUID antDesignBaseComponentUuid,
							@NonNull AntDesignLayoutType antDesignLayoutType,
							@NonNull AntDesignLayoutProps antDesignLayoutProps,
							@NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name(antDesignLayoutType.getType())
						.props(antDesignLayoutProps)
						.children(
								children.stream().map(AntDesignBaseComponent::getComponent).collect(toList())
						)
						.build(),
				true
		);
		layoutType = antDesignLayoutType;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private AntDesignLayoutType layoutType = null;

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignLayoutProps.AntDesignLayoutPropsBuilder propsBuilder = AntDesignLayoutProps.builder();

		public AntDesignLayout build() {
			propsBuilder.key(key);
			return new AntDesignLayout(
					antDesignBaseComponentUuid,
					layoutType,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignBaseComponent antDesignComponent) {
			if (antDesignComponent instanceof AntDesignLayout) {
				final AntDesignLayout layout = (AntDesignLayout) antDesignComponent;
				if (layout.layoutType == AntDesignLayoutType.SIDER) {
					propsBuilder.hasSider(true);
				}
			}
			children.add(antDesignComponent);
			return this;
		}

		public Builder turnOnCollapse() {
			final String id = AntDesignLayout.class.getSimpleName() + "-" + UUID.randomUUID();
			children.add( // technical tag for search by id
					Tag.builder()
							.setAttribute("id", id)
							.build(TagName.SPAN)
			);
			final JsFunction jsFunction = JsFunction.builder()
					.argumentNames(Collections.singletonList(
							"collapsed"
					))
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnCollapseEvent.NAME + "\");",
							"customEvent.collapsed = collapsed;",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementById(\"" + id + "\")",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onCollapse(jsFunction);
			return this;
		}

		public Builder layoutType(@NonNull AntDesignLayoutType layoutType) {
			this.layoutType = layoutType;
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}

		public Builder collapsible(boolean collapsible) {
			propsBuilder.collapsible(collapsible);
			return this;
		}
	}
}
