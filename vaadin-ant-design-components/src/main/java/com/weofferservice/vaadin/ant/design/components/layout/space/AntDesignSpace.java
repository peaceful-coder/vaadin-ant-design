package com.weofferservice.vaadin.ant.design.components.layout.space;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.AntDesignDirectionType;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignSpace extends AntDesignBaseComponent {

	private AntDesignSpace(@NonNull UUID antDesignBaseComponentUuid,
						   @NonNull AntDesignSpaceProps antDesignSpaceProps,
						   @NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Space")
						.props(antDesignSpaceProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignSpaceProps.AntDesignSpacePropsBuilder propsBuilder = AntDesignSpaceProps.builder();

		public AntDesignSpace build() {
			propsBuilder.key(key);
			return new AntDesignSpace(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder split(@NonNull AntDesignBaseComponent antDesignComponent) {
			propsBuilder.split(antDesignComponent.getComponent());
			return this;
		}

		public Builder align(@NonNull AntDesignAlign antDesignAlign) {
			propsBuilder.align(antDesignAlign.getAlign());
			return this;
		}

		public Builder size(@NonNull AntDesignSizeType antDesignSizeType) {
			propsBuilder.size(antDesignSizeType.getSize());
			return this;
		}

		public Builder direction(@NonNull AntDesignDirectionType antDesignDirectionType) {
			propsBuilder.direction(antDesignDirectionType.getDirection());
			return this;
		}

		public Builder addChild(@NonNull AntDesignBaseComponent antDesignComponent) {
			children.add(antDesignComponent);
			return this;
		}
	}
}
