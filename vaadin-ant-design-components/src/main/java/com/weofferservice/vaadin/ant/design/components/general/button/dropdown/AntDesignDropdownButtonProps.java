package com.weofferservice.vaadin.ant.design.components.general.button.dropdown;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.button.AntDesignButtonProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignDropdownButtonProps extends AntDesignButtonProps {

	ReactComponent overlay;

	JsFunction onClick;

	JsFunction onVisibleChange;
}
