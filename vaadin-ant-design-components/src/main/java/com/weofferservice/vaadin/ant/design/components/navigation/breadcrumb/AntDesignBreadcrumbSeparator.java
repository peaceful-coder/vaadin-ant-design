package com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagName;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignBreadcrumbSeparator extends AntDesignBaseComponent {

	private AntDesignBreadcrumbSeparator(@NonNull UUID antDesignBaseComponentUuid,
										 @NonNull AntDesignBreadcrumbSeparatorProps antDesignBreadcrumbSeparatorProps,
										 @NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("BreadcrumbSeparator")
						.props(antDesignBreadcrumbSeparatorProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				false
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignBreadcrumbSeparatorProps.AntDesignBreadcrumbSeparatorPropsBuilder propsBuilder =
				AntDesignBreadcrumbSeparatorProps.builder();

		public AntDesignBreadcrumbSeparator build() {
			if (children.isEmpty()) {
				children.add(
						Tag.builder()
								.text("/")
								.build(TagName.SPAN));
			}
			propsBuilder.key(key);
			return new AntDesignBreadcrumbSeparator(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignBaseComponent child) {
			children.add(child);
			return this;
		}
	}
}
