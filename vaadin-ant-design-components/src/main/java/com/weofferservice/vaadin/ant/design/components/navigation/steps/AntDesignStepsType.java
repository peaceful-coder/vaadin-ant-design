package com.weofferservice.vaadin.ant.design.components.navigation.steps;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignStepsType {
	DEFAULT("default"),
	NAVIGATION("navigation");

	@Getter
	private final String type;
}
