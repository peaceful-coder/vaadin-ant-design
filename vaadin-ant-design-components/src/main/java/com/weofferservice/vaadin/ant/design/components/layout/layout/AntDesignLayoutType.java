package com.weofferservice.vaadin.ant.design.components.layout.layout;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignLayoutType {
	LAYOUT("Layout"),
	HEADER("Header"),
	SIDER("Sider"),
	CONTENT("Content"),
	FOOTER("Footer");

	@Getter
	private final String type;
}
