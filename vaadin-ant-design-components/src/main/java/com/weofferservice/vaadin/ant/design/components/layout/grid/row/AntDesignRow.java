package com.weofferservice.vaadin.ant.design.components.layout.grid.row;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.layout.grid.col.AntDesignCol;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignRow extends AntDesignBaseComponent {

	private AntDesignRow(@NonNull UUID antDesignBaseComponentUuid,
						 @NonNull AntDesignRowProps antDesignRowProps,
						 @NonNull List<AntDesignCol> columns) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Row")
						.props(antDesignRowProps)
						.children(columns.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private AntDesignGutterProps horizontalGutter = null;

		private AntDesignGutterProps verticalGutter = null;

		private final List<AntDesignCol> columns = new ArrayList<>();

		private final AntDesignRowProps.AntDesignRowPropsBuilder propsBuilder = AntDesignRowProps.builder();

		public AntDesignRow build() {
			final List<AntDesignGutterProps> gutter = new ArrayList<>();
			if (Objects.nonNull(horizontalGutter)) {
				gutter.add(horizontalGutter);
			}
			if (Objects.nonNull(verticalGutter)) {
				if (Objects.isNull(horizontalGutter)) {
					gutter.add(AntDesignGutterProps.builder().build());
				}
				gutter.add(verticalGutter);
			}
			propsBuilder.key(key);
			return new AntDesignRow(
					antDesignBaseComponentUuid,
					propsBuilder.gutter(gutter).build(),
					columns);
		}

		public Builder justify(@NonNull AntDesignRowJustify antDesignRowJustify) {
			propsBuilder.justify(antDesignRowJustify.getJustify());
			return this;
		}

		public Builder align(@NonNull AntDesignRowAlign antDesignRowAlign) {
			propsBuilder.align(antDesignRowAlign.getAlign());
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder wrap(boolean wrap) {
			propsBuilder.wrap(wrap);
			return this;
		}

		public Builder horizontalGutter(int xs, int sm, int md, int lg) {
			horizontalGutter =
					AntDesignGutterProps.builder()
							.xs(xs)
							.sm(sm)
							.md(md)
							.lg(lg)
							.build();
			return this;
		}

		public Builder verticalGutter(int xs, int sm, int md, int lg) {
			verticalGutter =
					AntDesignGutterProps.builder()
							.xs(xs)
							.sm(sm)
							.md(md)
							.lg(lg)
							.build();
			return this;
		}

		public Builder addColumn(@NonNull AntDesignCol column) {
			columns.add(column);
			return this;
		}
	}
}
