package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignMenuProps extends ReactProps {

	String mode;

	String theme;

	Boolean inlineCollapsed;

	List<UUID> selectedKeys;

	List<UUID> defaultSelectedKeys;

	List<UUID> openKeys;

	List<UUID> defaultOpenKeys;

	JsFunction onClick;
}
