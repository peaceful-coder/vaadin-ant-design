package com.weofferservice.vaadin.ant.design.components.navigation.pagination;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.navigation.AntDesignSize;
import lombok.NonNull;

import java.util.Map;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignPagination extends AntDesignBaseComponent {

	private AntDesignPagination(@NonNull UUID antDesignBaseComponentUuid,
								@NonNull AntDesignPaginationProps antDesignPaginationProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Pagination")
						.props(antDesignPaginationProps)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignPaginationProps.AntDesignPaginationPropsBuilder propsBuilder = AntDesignPaginationProps.builder();

		public AntDesignPagination build() {
			propsBuilder.key(key);
			return new AntDesignPagination(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder current(int current) {
			propsBuilder.current(current);
			return this;
		}

		public Builder defaultCurrent(int defaultCurrent) {
			propsBuilder.defaultCurrent(defaultCurrent);
			return this;
		}

		public Builder defaultPageSize(int defaultPageSize) {
			propsBuilder.defaultPageSize(defaultPageSize);
			return this;
		}

		public Builder total(int total) {
			propsBuilder.total(total);
			return this;
		}

		public Builder showSizeChanger(boolean showSizeChanger) {
			propsBuilder.showSizeChanger(showSizeChanger);
			return this;
		}

		public Builder disabled(boolean disabled) {
			propsBuilder.disabled(disabled);
			return this;
		}

		public Builder showQuickJumper(boolean showQuickJumper) {
			propsBuilder.showQuickJumper(showQuickJumper);
			return this;
		}

		public Builder simple(boolean simple) {
			propsBuilder.simple(simple);
			return this;
		}

		public Builder size(@NonNull AntDesignSize antDesignSize) {
			propsBuilder.size(antDesignSize.getSize());
			return this;
		}

		public Builder className(@NonNull String className) {
			propsBuilder.className(className);
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}
	}
}
