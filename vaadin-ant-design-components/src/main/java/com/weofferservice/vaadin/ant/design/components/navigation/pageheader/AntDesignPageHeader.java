package com.weofferservice.vaadin.ant.design.components.navigation.pageheader;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.event.AntDesignOnBackEvent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignPageHeader extends AntDesignBaseComponent {

	private AntDesignPageHeader(@NonNull UUID antDesignBaseComponentUuid,
								@NonNull AntDesignPageHeaderProps antDesignPageHeaderProps) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("PageHeader")
						.props(antDesignPageHeaderProps)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private String className = null;

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final List<AntDesignBaseComponent> tags = new ArrayList<>();

		private final List<AntDesignRoute> routes = new ArrayList<>();

		private final AntDesignPageHeaderProps.AntDesignPageHeaderPropsBuilder propsBuilder = AntDesignPageHeaderProps.builder();

		public AntDesignPageHeader build() {
			propsBuilder.className(className);
			propsBuilder.key(key);
			propsBuilder.extra(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()));
			propsBuilder.breadcrumb(AntDesignBreadcrumb.builder().routes(routes).build());
			propsBuilder.tags(tags.stream().map(AntDesignBaseComponent::getComponent).collect(toList()));
			return new AntDesignPageHeader(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder addChild(@NonNull AntDesignBaseComponent child) {
			children.add(child);
			return this;
		}

		public Builder turnOnBack() {
			final String classNameForReference = AntDesignPageHeader.class.getSimpleName() + "-" + UUID.randomUUID();
			final JsFunction jsFunction = JsFunction.builder()
					.bodyLines(Arrays.asList(
							"const customEvent = new CustomEvent(\"" + AntDesignOnBackEvent.NAME + "\");",
							"customEvent.antDesignBaseComponentUuid = \"" + antDesignBaseComponentUuid + "\";",
							"customEvent.key = \"" + key + "\";",
							"document",
							"\t.getElementsByClassName(\"" + classNameForReference + "\")[0]",
							"\t.closest(\"" + ANT_DESIGN_COMPONENT_TAG_NAME + "\")",
							"\t.dispatchEvent(customEvent);"
					))
					.build();
			propsBuilder.onBack(jsFunction);
			className(classNameForReference);
			return this;
		}

		public Builder title(@NonNull String title) {
			propsBuilder.title(title);
			return this;
		}

		public Builder subTitle(@NonNull String subTitle) {
			propsBuilder.subTitle(subTitle);
			return this;
		}

		public Builder ghost(boolean ghost) {
			propsBuilder.ghost(ghost);
			return this;
		}

		public Builder addTag(@NonNull AntDesignBaseComponent antDesignBaseComponent) {
			tags.add(antDesignBaseComponent);
			return this;
		}

		public Builder avatar(@NonNull String src) {
			propsBuilder.avatar(AntDesignAvatar.builder().src(src).build());
			return this;
		}

		public Builder addBreadcrumb(@NonNull String path, @NonNull String breadcrumbName) {
			routes.add(
					AntDesignRoute.builder()
							.path(path)
							.breadcrumbName(breadcrumbName)
							.build());
			return this;
		}

		public Builder footer(@NonNull AntDesignBaseComponent antDesignBaseComponent) {
			propsBuilder.footer(antDesignBaseComponent.getComponent());
			return this;
		}

		public Builder className(@NonNull String className) {
			if (Objects.isNull(this.className)) {
				this.className = className;
			} else {
				this.className += " " + className;
			}
			return this;
		}

		public Builder style(@NonNull Map<String, Object> style) {
			propsBuilder.style(style);
			return this;
		}
	}
}
