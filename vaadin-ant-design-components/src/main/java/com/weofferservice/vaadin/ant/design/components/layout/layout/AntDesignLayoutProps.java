package com.weofferservice.vaadin.ant.design.components.layout.layout;

import com.weofferservice.vaadin.ant.design.components.JsFunction;
import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignLayoutProps extends ReactProps {

	Boolean hasSider;

	Boolean collapsible;

	JsFunction onCollapse;
}
