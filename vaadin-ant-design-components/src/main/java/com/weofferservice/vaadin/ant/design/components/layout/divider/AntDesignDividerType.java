package com.weofferservice.vaadin.ant.design.components.layout.divider;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignDividerType {
	HORIZONTAL("horizontal"),
	VERTICAL("vertical");

	@Getter
	private final String type;
}
