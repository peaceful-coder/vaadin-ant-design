package com.weofferservice.vaadin.ant.design.components;

import lombok.Builder;
import lombok.Value;

import java.util.List;

/**
 * @author Peaceful Coder
 */
@Value
@Builder
public class ReactComponent implements JsObject {

	String name;

	ReactProps props;

	List<ReactComponent> children;
}
