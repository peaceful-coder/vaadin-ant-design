package com.weofferservice.vaadin.ant.design.components.general.typography.paragraph;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import com.weofferservice.vaadin.ant.design.components.general.typography.AntDesignBaseProps;
import lombok.Value;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@SuperBuilder
public class AntDesignParagraphProps extends AntDesignBaseProps {

	ReactProps editable;

	ReactProps copyable;

	ReactProps ellipsis;
}
