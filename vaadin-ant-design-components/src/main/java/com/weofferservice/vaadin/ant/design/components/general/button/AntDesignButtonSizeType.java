package com.weofferservice.vaadin.ant.design.components.general.button;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignButtonSizeType {
	DEFAULT(null),
	SMALL("small"),
	MIDDLE("middle"),
	LARGE("large");

	@Getter
	private final String sizeType;
}
