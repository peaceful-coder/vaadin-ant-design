package com.weofferservice.vaadin.ant.design.components.navigation.breadcrumb;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * @author Peaceful Coder
 */
public class AntDesignBreadcrumb extends AntDesignBaseComponent {

	private AntDesignBreadcrumb(@NonNull UUID antDesignBaseComponentUuid,
								@NonNull AntDesignBreadcrumbProps antDesignBreadcrumbProps,
								@NonNull List<AntDesignBaseComponent> children) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Breadcrumb")
						.props(antDesignBreadcrumbProps)
						.children(children.stream().map(AntDesignBaseComponent::getComponent).collect(toList()))
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final List<AntDesignBaseComponent> children = new ArrayList<>();

		private final AntDesignBreadcrumbProps.AntDesignBreadcrumbPropsBuilder propsBuilder =
				AntDesignBreadcrumbProps.builder();

		public AntDesignBreadcrumb build() {
			propsBuilder.key(key);
			return new AntDesignBreadcrumb(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					children);
		}

		public Builder addChild(@NonNull AntDesignBaseComponent antDesignComponent) {
			children.add(antDesignComponent);
			return this;
		}

		public Builder separator(@NonNull String separator) {
			propsBuilder.separator(separator);
			return this;
		}
	}
}
