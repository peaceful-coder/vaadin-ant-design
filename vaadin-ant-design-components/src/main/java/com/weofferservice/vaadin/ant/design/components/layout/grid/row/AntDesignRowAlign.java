package com.weofferservice.vaadin.ant.design.components.layout.grid.row;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Peaceful Coder
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum AntDesignRowAlign {
	TOP("top"),
	MIDDLE("middle"),
	BOTTOM("bottom"),
	STRETCH("stretch");

	@Getter
	private final String align;
}
