package com.weofferservice.vaadin.ant.design.components.general.typography.link;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import lombok.NonNull;

import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignLink extends AntDesignBaseComponent {

	private AntDesignLink(@NonNull UUID antDesignBaseComponentUuid,
						  @NonNull AntDesignLinkProps props) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("Link")
						.props(props)
						.build(),
				true
		);
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private final AntDesignLinkProps.AntDesignLinkPropsBuilder propsBuilder = AntDesignLinkProps.builder();

		public AntDesignLink build() {
			propsBuilder.key(key);
			return new AntDesignLink(
					antDesignBaseComponentUuid,
					propsBuilder.build());
		}

		public Builder text(@NonNull String text) {
			propsBuilder.children(text);
			return this;
		}

		public Builder href(@NonNull String href) {
			propsBuilder.href(href);
			return this;
		}

		public Builder target(@NonNull String target) {
			propsBuilder.target(target);
			return this;
		}
	}
}
