package com.weofferservice.vaadin.ant.design.components.navigation.menu;

import com.weofferservice.vaadin.ant.design.components.AntDesignBaseComponent;
import com.weofferservice.vaadin.ant.design.components.ReactComponent;
import com.weofferservice.vaadin.ant.design.components.general.icon.AntDesignIcon;
import com.weofferservice.vaadin.ant.design.components.tag.DangerouslySetInnerHTMLProps;
import com.weofferservice.vaadin.ant.design.components.tag.Tag;
import com.weofferservice.vaadin.ant.design.components.tag.TagProps;
import lombok.Getter;
import lombok.NonNull;

import java.util.Collections;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
public class AntDesignMenuItem extends AntDesignBaseComponent {

	@Getter
	private final String title;

	private AntDesignMenuItem(@NonNull UUID antDesignBaseComponentUuid,
							  @NonNull AntDesignMenuItemProps antDesignMenuItemProps,
							  @NonNull Tag tag) {
		super(
				antDesignBaseComponentUuid,
				ReactComponent.builder()
						.name("MenuItem")
						.props(antDesignMenuItemProps)
						.children(Collections.singletonList(tag.getComponent()))
						.build(),
				false
		);
		final DangerouslySetInnerHTMLProps dangerouslySetInnerHTML =
				(DangerouslySetInnerHTMLProps) ((TagProps) tag.getComponent().getProps()).getDangerouslySetInnerHTML();
		this.title = Objects.nonNull(dangerouslySetInnerHTML) ? dangerouslySetInnerHTML.get__html() : null;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private final UUID antDesignBaseComponentUuid = UUID.randomUUID();

		private final UUID key = UUID.randomUUID();

		private Tag tag;

		private final AntDesignMenuItemProps.AntDesignMenuItemPropsBuilder propsBuilder = AntDesignMenuItemProps.builder();

		public AntDesignMenuItem build() {
			propsBuilder.key(key);
			return new AntDesignMenuItem(
					antDesignBaseComponentUuid,
					propsBuilder.build(),
					tag);
		}

		public Builder title(@NonNull String title) {
			propsBuilder.title(title);
			return this;
		}

		public Builder danger(@NonNull Boolean danger) {
			propsBuilder.danger(danger);
			return this;
		}

		public Builder tag(@NonNull Tag tag) {
			this.tag = tag;
			return this;
		}

		public Builder icon(@NonNull AntDesignIcon antDesignIcon) {
			propsBuilder.icon(antDesignIcon.getComponent());
			return this;
		}

		public Builder disabled(boolean disabled) {
			propsBuilder.disabled(disabled);
			return this;
		}
	}
}
