package com.weofferservice.vaadin.ant.design.components;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.dependency.NpmPackage;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;
import lombok.NonNull;

import java.util.Objects;
import java.util.UUID;

/**
 * @author Peaceful Coder
 */
@Tag(AntDesignBaseComponent.ANT_DESIGN_COMPONENT_TAG_NAME)
@NpmPackage(value = "react", version = "^17.0.1")
@NpmPackage(value = "react-dom", version = "^17.0.1")
@NpmPackage(value = "antd", version = "^4.8.4")
@NpmPackage(value = "rc-menu", version = "^8.10.1")
@NpmPackage(value = "@ant-design/icons", version = "^4.3.0")
@NpmPackage(value = "@babel/plugin-transform-react-jsx", version = "^7.12.5")
@NpmPackage(value = "@babel/core", version = "^7.12.3")
@NpmPackage(value = "babel-loader", version = "^8.2.1")
@NpmPackage(value = "@svgr/webpack", version = "^5.5.0")
@CssImport("antd/dist/antd.css")
@JsModule("@vaadin/flow-frontend/ant-design-component.js")
public abstract class AntDesignBaseComponent extends Component implements HasStyle, HasSize {

	protected static final String ANT_DESIGN_COMPONENT_TAG_NAME = "ant-design-component";

	private final ObjectMapper objectMapper =
			new ObjectMapper()
					.setSerializationInclusion(JsonInclude.Include.NON_NULL)
					.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

	@Getter
	private final ReactComponent component;

	@Getter
	private final UUID uuid;

	public AntDesignBaseComponent(@NonNull UUID uuid,
								  @NonNull ReactComponent component,
								  boolean needRender) {
		this.uuid = uuid;
		this.component = component;
		if (needRender) {
			final String id = getClass().getSimpleName() + "-" + UUID.randomUUID().toString();
			setId(id);
			final String arguments = String.format(
					"\"%s\", %s",
					id, transformReactComponentToString(component)
			);
			UI.getCurrent().getPage().executeJs(
					"renderAntDesignComponent(" + arguments + ");");
		}
	}

	public UUID getKey() {
		if (Objects.isNull(component.getProps())) {
			throw new UnsupportedOperationException("Could not get props");
		}
		return component.getProps().getKey();
	}

	private String transformReactComponentToString(@NonNull Object component) {
		try {
			return objectMapper.writeValueAsString(component);
		} catch (JsonProcessingException e) {
			throw new IllegalStateException(e);
		}
	}

	public <T extends ComponentEvent<?>> Registration addListener(@NonNull Class<T> eventType,
																  @NonNull ComponentEventListener<T> listener) {
		return super.addListener(eventType, listener);
	}
}
