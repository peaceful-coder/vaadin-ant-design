package com.weofferservice.vaadin.ant.design.components.general.typography;

import com.weofferservice.vaadin.ant.design.components.ReactProps;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;

/**
 * @author Peaceful Coder
 */
@Value
@NonFinal
@SuperBuilder
public class AntDesignBaseProps extends ReactProps {

	String children;

	String type;

	Boolean disabled;

	Boolean mark;

	Boolean code;

	Boolean keyboard;

	Boolean underline;

	Boolean delete;

	Boolean strong;
}
